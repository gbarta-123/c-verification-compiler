#ifndef PROGRAM_H
#define PROGRAM_H

#include <stddef.h>
#include "Function.h"
#include "defines.h"
#include "ArExpr.h"

enum status { AREXPR1, AREXPR2, FUNC, PCASSUME, PCASSIGN};

struct StatusStack_ {
	enum status st[100];
	int sp;
};

StatusStack mk_statusstack();
void push_status(StatusStack, enum status);
enum status pop_status(StatusStack);
enum status get_top_stack_status(StatusStack);

ArExpr current_expr1;
ArExpr current_expr2;

struct PContainer_ {
	Function *funcs;
	StatusStack stst;
	size_t n_funcs;
	size_t reall_q;
};

PContainer mk_pcontainer();
void add_func_pcontainer(PContainer, Function);
void print_pcontainer(PContainer);

void add_int_pcont(PContainer, int);
void add_oper_pcont(PContainer, Operator);
void submit_erexpr_pcont(PContainer);
#endif // PROGRAM_H
