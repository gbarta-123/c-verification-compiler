#ifndef AREXPR_H
#define AREXPR_H

#include <stddef.h>
#include "Expr.h"
#include "defines.h"

struct ArExpr_ {
	Expr *exprs;
	size_t length;
	size_t reall_q;
};

ArExpr mk_arexpr();
void add_expr_arexpr(ArExpr, Expr);

void print_arexpr(ArExpr);

#endif // AREXPR_H
