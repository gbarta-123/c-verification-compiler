#include <stdlib.h>
#include <stdio.h>
#include "Statement.h"
#include "Mem.h"

Statement mk_assume_statement(ArExpr _stmt1, Operator _op, ArExpr _stmt2)
{
	Statement stmt = pmalloc(sizeof(*stmt));
	stmt->type = ASSUME;
	stmt->u.assume.stmt1 = _stmt1;
	stmt->u.assume.op = _op;
	stmt->u.assume.stmt2 = _stmt2;
	return stmt;
}

Statement mk_assign_statement(ArExpr _lhs, ArExpr _rhs)
{
	Statement stmt = pmalloc(sizeof(*stmt));
	stmt->type = ASSIGN;
	stmt->u.assign.lhs = _lhs;
	stmt->u.assign.rhs = _rhs;
	return stmt;
}

void print_statement(Statement stmt)
{
	switch(stmt->type) {
	case ASSIGN:
		print_arexpr(stmt->u.assign.lhs);
		printf(" = ");
		print_arexpr(stmt->u.assign.rhs);
		break;
	case ASSUME:
		print_arexpr(stmt->u.assume.stmt1);
		print_operator(stmt->u.assume.op);
		print_arexpr(stmt->u.assume.stmt2);
		break;
	default:
		fprintf(stderr, "UNDEFINED STATEMENT TYPE!\n");
		exit(EXIT_FAILURE);
	}
}
