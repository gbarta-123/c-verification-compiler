#ifndef STATEMENT_H
#define STATEMENT_H

#include "ArExpr.h"
#include "Operators.h"
#include "defines.h"

struct Statement_ {
	enum { ASSUME, ASSIGN } type;
	union {
		struct {
			ArExpr stmt1;
			Operator op;
			ArExpr stmt2;
		} assume;
		struct {
			ArExpr lhs;
			ArExpr rhs;
		} assign;
	} u;
};

Statement mk_assume_statement(ArExpr, Operator, ArExpr);
Statement mk_assign_statement(ArExpr, ArExpr);

void print_statement(Statement);

#endif // STATEMENT_H
