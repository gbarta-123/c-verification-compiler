#ifndef PATH_H
#define PATH_H

#include <stddef.h>
#include "Formula.h"
#include "Statement.h"
#include "defines.h"

struct Path_ {
	Formula precond, postcond;
	Statement *stmts;
	size_t n_stmts;
};

Path mk_path(Formula, Formula, Statement *, size_t);
void add_stmt_path(Path, Statement);
void print_path(Path);
#endif // PATH_H
