#ifndef MEM_HEADER
#define MEM_HEADER

/**
 * \brief Deals with Prover's memory management.
 */
#include <stddef.h>

/**
 * \brief Works exactly like malloc but it if can't allocate
 *        the memory needed, it exits with an error message.
 */
void *pmalloc(size_t size);
/**
 * \brief Works exactly like free but it ptr is already
 *        freed, it exits with an error message.
 */
void pfree(void *ptr);
/**
 * \brief Works exactly like realloc but it if can't allocate
 *        the memory needed, it exits with an error message.
 */
void *prealloc(void *ptr, size_t size);

#endif // MEM_HEADER
