#ifndef CONSTANT_H
#define CONSTANT_H

#include "Bitvect.h"
#include "defines.h"

struct Constant_ {
	enum { NAMED, UNNAMED } type;
	union {
		struct {
			bitvect_t value;
			char *name;
		} named;
		struct {
			bitvect_t value;
		} unnamed;
	} u;
};

Constant mk_named_constant(bitvect_t, char *);
Constant mk_unnamed_constant(bitvect_t);

void print_constant(Constant);
#endif // CONSTANT_H
