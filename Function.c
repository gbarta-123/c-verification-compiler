#include <string.h>
#include "Function.h"
#include "Mem.h"


Function mk_function(char *_name)
{
	Function function = pmalloc(sizeof(*function));
	function->path = pmalloc(sizeof(*(function->path)) * REALLOC_QUANTUM);
	function->name = _name;
	function->n_path = 0;
	function->reall_q = 1;
	return function;
}

void add_path_function(Function _func, Path _path)
{
	// P(need to expand _func->path) = 1 / REALLOC_QUANTUM
	if(unlikely((REALLOC_QUANTUM * _func->reall_q) < (_func->n_path + 1))) {
		size_t new_length = sizeof(*(_func->path))
			* (++(_func->reall_q))
			* REALLOC_QUANTUM;
	   // Allocate a new quantum for _func->n_path
		prealloc(_func->path, new_length);
	}
	// Add a new path to _func->path array
	_func->path[_func->n_path++] = _path;
}

void print_function(Function func)
{
	for(size_t i = 0; i < func->n_path; ++i)
		print_path(func->path[i]);
}
