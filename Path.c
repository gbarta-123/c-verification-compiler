#include <stdio.h>
#include "Path.h"
#include "Mem.h"
#include "Statement.h"

Path mk_path(Formula _precond, Formula _postcond,
	     Statement *_stmts, size_t _n_stmts)
{
	Path path = pmalloc(sizeof(*path));
	path->precond = _precond;
	path->postcond = _postcond;
	path->stmts = _stmts;
	path->n_stmts = _n_stmts;
	return path;
}

void add_stmt_path(Path _path, Statement _stmt)
{
	prealloc(_path->stmts, _path->n_stmts + 1);
	_path->stmts[_path->n_stmts++] = _stmt;
}

void print_path(Path path)
{
	// TODO print pre&postcond
	printf("\n[PATH]\n");
	for(size_t i = 0; i < path->n_stmts; ++i)
		print_statement(path->stmts[i]);
}
