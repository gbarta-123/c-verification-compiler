#include <stdio.h>
#include <stdlib.h>
#include "Operators.h"
#include "Mem.h"

Operator mk_relop_operator(enum relop _rel)
{
	Operator operator = pmalloc(sizeof(*operator));
	operator->type = REL;
	operator->u.rel = _rel;
	return operator;
}

Operator mk_arop_operator(enum arop _ar)
{
	Operator operator = pmalloc(sizeof(*operator));
	operator->type = AR;
	operator->u.ar = _ar;
	return operator;
}

void print_operator(Operator oper)
{
	switch(oper->type) {
	case(REL):
		__print_relop(oper->u.rel);
		break;
	case(AR):
		__print_arop(oper->u.ar);
		break;
	case(LOG):
		__print_logop(oper->u.log);
		break;
	default:
		fprintf(stderr, "OPERATOR TYPE NOT DEFINED!\n");
		exit(EXIT_FAILURE);
	}
}

void __print_relop(enum relop oper)
{
	switch(oper) {
	case(GT):
		printf(" > ");
		break;
	case(LT):
		printf(" < ");
		break;
	case(GTE):
		printf(" >=  ");
		break;
	case(LTE):
		printf(" <= ");
		break;
	default:
		fprintf(stderr, "RELATIONAL OPERATOR NOT DEFINED!\n");
		exit(EXIT_FAILURE);
	}
}

void __print_arop(enum arop oper)
{
	switch(oper) {
	case PLUS:
		printf(" + ");
		break;
	case MINUS:
		printf(" - ");
		break;
	case MUL:
		printf(" * ");
		break;
	default:
		fprintf(stderr, "ARITHMETIC OPERATOR NOT DEFINED!\n");
		exit(EXIT_FAILURE);
	}
}

void __print_logop(enum logop oper)
{
	switch(oper) {
	case AND:
		printf(" AND ");
		break;
	case OR:
		printf(" OR ");
		break;
	case NOT:
		printf(" NOT ");
		break;
	case IMPL:
		printf(" -> ");
		break;
	case IFF:
		printf(" <-> ");
		break;
	default:
		fprintf(stderr, "LOGICAL OPERATOR NOT DEFINED!\n");
		exit(EXIT_FAILURE);
	}
}
