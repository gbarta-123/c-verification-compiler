#ifndef FUNCTION_H
#define FUNCTION_H

#include <stddef.h>
#include "defines.h"
#include "Path.h"

struct Function_ {
	char *name;
	Path *path;
	size_t n_path; // The number of path in the function
	size_t reall_q; // The length of the path array is reall_q * REALLOC_QUANTUM
};

Function mk_function(char *);
void add_path_function(Function, Path);
void print_function(Function);
#endif // FUNCTION_H
