/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         c_parse
#define yylex           c_lex
#define yyerror         c_error
#define yydebug         c_debug
#define yynerrs         c_nerrs

#define yylval          c_lval
#define yychar          c_char

/* Copy the first part of user declarations.  */
#line 2 "c.y" /* yacc.c:339  */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Absyn.h"
#define initialize_lexer c__initialize_lexer
extern int yyparse(void);
extern int yylex(void);
int yy_mylinenumber;
extern int initialize_lexer(FILE * inp);
void yyerror(const char *str)
{
  extern char *c_text;
  fprintf(stderr,"error: line %d: %s at %s\n",
    yy_mylinenumber + 1, str, c_text);
}

Program YY_RESULT_Program_ = 0;
Program pProgram(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Program_;
  }
}

ListExternal_declaration YY_RESULT_ListExternal_declaration_ = 0;
ListExternal_declaration pListExternal_declaration(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListExternal_declaration_;
  }
}

External_declaration YY_RESULT_External_declaration_ = 0;
External_declaration pExternal_declaration(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_External_declaration_;
  }
}

Function_def YY_RESULT_Function_def_ = 0;
Function_def pFunction_def(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Function_def_;
  }
}

Dec YY_RESULT_Dec_ = 0;
Dec pDec(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Dec_;
  }
}

ListDec YY_RESULT_ListDec_ = 0;
ListDec pListDec(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListDec_;
  }
}

ListDeclaration_specifier YY_RESULT_ListDeclaration_specifier_ = 0;
ListDeclaration_specifier pListDeclaration_specifier(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListDeclaration_specifier_;
  }
}

Declaration_specifier YY_RESULT_Declaration_specifier_ = 0;
Declaration_specifier pDeclaration_specifier(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Declaration_specifier_;
  }
}

ListInit_declarator YY_RESULT_ListInit_declarator_ = 0;
ListInit_declarator pListInit_declarator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListInit_declarator_;
  }
}

Init_declarator YY_RESULT_Init_declarator_ = 0;
Init_declarator pInit_declarator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Init_declarator_;
  }
}

Type_specifier YY_RESULT_Type_specifier_ = 0;
Type_specifier pType_specifier(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Type_specifier_;
  }
}

Storage_class_specifier YY_RESULT_Storage_class_specifier_ = 0;
Storage_class_specifier pStorage_class_specifier(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Storage_class_specifier_;
  }
}

Type_qualifier YY_RESULT_Type_qualifier_ = 0;
Type_qualifier pType_qualifier(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Type_qualifier_;
  }
}

Struct_or_union_spec YY_RESULT_Struct_or_union_spec_ = 0;
Struct_or_union_spec pStruct_or_union_spec(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Struct_or_union_spec_;
  }
}

Struct_or_union YY_RESULT_Struct_or_union_ = 0;
Struct_or_union pStruct_or_union(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Struct_or_union_;
  }
}

ListStruct_dec YY_RESULT_ListStruct_dec_ = 0;
ListStruct_dec pListStruct_dec(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListStruct_dec_;
  }
}

Struct_dec YY_RESULT_Struct_dec_ = 0;
Struct_dec pStruct_dec(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Struct_dec_;
  }
}

ListSpec_qual YY_RESULT_ListSpec_qual_ = 0;
ListSpec_qual pListSpec_qual(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListSpec_qual_;
  }
}

Spec_qual YY_RESULT_Spec_qual_ = 0;
Spec_qual pSpec_qual(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Spec_qual_;
  }
}

ListStruct_declarator YY_RESULT_ListStruct_declarator_ = 0;
ListStruct_declarator pListStruct_declarator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListStruct_declarator_;
  }
}

Struct_declarator YY_RESULT_Struct_declarator_ = 0;
Struct_declarator pStruct_declarator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Struct_declarator_;
  }
}

Enum_specifier YY_RESULT_Enum_specifier_ = 0;
Enum_specifier pEnum_specifier(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Enum_specifier_;
  }
}

ListEnumerator YY_RESULT_ListEnumerator_ = 0;
ListEnumerator pListEnumerator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListEnumerator_;
  }
}

Enumerator YY_RESULT_Enumerator_ = 0;
Enumerator pEnumerator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Enumerator_;
  }
}

Declarator YY_RESULT_Declarator_ = 0;
Declarator pDeclarator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Declarator_;
  }
}

Direct_declarator YY_RESULT_Direct_declarator_ = 0;
Direct_declarator pDirect_declarator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Direct_declarator_;
  }
}

Pointer YY_RESULT_Pointer_ = 0;
Pointer pPointer(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Pointer_;
  }
}

ListType_qualifier YY_RESULT_ListType_qualifier_ = 0;
ListType_qualifier pListType_qualifier(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListType_qualifier_;
  }
}

Parameter_type YY_RESULT_Parameter_type_ = 0;
Parameter_type pParameter_type(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Parameter_type_;
  }
}

Parameter_declarations YY_RESULT_Parameter_declarations_ = 0;
Parameter_declarations pParameter_declarations(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Parameter_declarations_;
  }
}

Parameter_declaration YY_RESULT_Parameter_declaration_ = 0;
Parameter_declaration pParameter_declaration(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Parameter_declaration_;
  }
}

ListIdent YY_RESULT_ListIdent_ = 0;
ListIdent pListIdent(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListIdent_;
  }
}

Initializer YY_RESULT_Initializer_ = 0;
Initializer pInitializer(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Initializer_;
  }
}

Initializers YY_RESULT_Initializers_ = 0;
Initializers pInitializers(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Initializers_;
  }
}

Type_name YY_RESULT_Type_name_ = 0;
Type_name pType_name(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Type_name_;
  }
}

Abstract_declarator YY_RESULT_Abstract_declarator_ = 0;
Abstract_declarator pAbstract_declarator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Abstract_declarator_;
  }
}

Dir_abs_dec YY_RESULT_Dir_abs_dec_ = 0;
Dir_abs_dec pDir_abs_dec(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Dir_abs_dec_;
  }
}

Stm YY_RESULT_Stm_ = 0;
Stm pStm(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Stm_;
  }
}

Labeled_stm YY_RESULT_Labeled_stm_ = 0;
Labeled_stm pLabeled_stm(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Labeled_stm_;
  }
}

Compound_stm YY_RESULT_Compound_stm_ = 0;
Compound_stm pCompound_stm(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Compound_stm_;
  }
}

Expression_stm YY_RESULT_Expression_stm_ = 0;
Expression_stm pExpression_stm(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Expression_stm_;
  }
}

Selection_stm YY_RESULT_Selection_stm_ = 0;
Selection_stm pSelection_stm(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Selection_stm_;
  }
}

Iter_stm YY_RESULT_Iter_stm_ = 0;
Iter_stm pIter_stm(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Iter_stm_;
  }
}

Jump_stm YY_RESULT_Jump_stm_ = 0;
Jump_stm pJump_stm(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Jump_stm_;
  }
}

ListStm YY_RESULT_ListStm_ = 0;
ListStm pListStm(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListStm_;
  }
}

Exp YY_RESULT_Exp_ = 0;
Exp pExp(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Exp_;
  }
}

Constant YY_RESULT_Constant_ = 0;
Constant pConstant(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Constant_;
  }
}

Constant_expression YY_RESULT_Constant_expression_ = 0;
Constant_expression pConstant_expression(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Constant_expression_;
  }
}

Unary_operator YY_RESULT_Unary_operator_ = 0;
Unary_operator pUnary_operator(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Unary_operator_;
  }
}

ListExp YY_RESULT_ListExp_ = 0;
ListExp pListExp(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListExp_;
  }
}

Assignment_op YY_RESULT_Assignment_op_ = 0;
Assignment_op pAssignment_op(FILE *inp)
{
  initialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Assignment_op_;
  }
}


ListExternal_declaration reverseListExternal_declaration(ListExternal_declaration l)
{
  ListExternal_declaration prev = 0;
  ListExternal_declaration tmp = 0;
  while (l)
  {
    tmp = l->listexternal_declaration_;
    l->listexternal_declaration_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListDec reverseListDec(ListDec l)
{
  ListDec prev = 0;
  ListDec tmp = 0;
  while (l)
  {
    tmp = l->listdec_;
    l->listdec_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListDeclaration_specifier reverseListDeclaration_specifier(ListDeclaration_specifier l)
{
  ListDeclaration_specifier prev = 0;
  ListDeclaration_specifier tmp = 0;
  while (l)
  {
    tmp = l->listdeclaration_specifier_;
    l->listdeclaration_specifier_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListInit_declarator reverseListInit_declarator(ListInit_declarator l)
{
  ListInit_declarator prev = 0;
  ListInit_declarator tmp = 0;
  while (l)
  {
    tmp = l->listinit_declarator_;
    l->listinit_declarator_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListStruct_dec reverseListStruct_dec(ListStruct_dec l)
{
  ListStruct_dec prev = 0;
  ListStruct_dec tmp = 0;
  while (l)
  {
    tmp = l->liststruct_dec_;
    l->liststruct_dec_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListSpec_qual reverseListSpec_qual(ListSpec_qual l)
{
  ListSpec_qual prev = 0;
  ListSpec_qual tmp = 0;
  while (l)
  {
    tmp = l->listspec_qual_;
    l->listspec_qual_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListStruct_declarator reverseListStruct_declarator(ListStruct_declarator l)
{
  ListStruct_declarator prev = 0;
  ListStruct_declarator tmp = 0;
  while (l)
  {
    tmp = l->liststruct_declarator_;
    l->liststruct_declarator_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListEnumerator reverseListEnumerator(ListEnumerator l)
{
  ListEnumerator prev = 0;
  ListEnumerator tmp = 0;
  while (l)
  {
    tmp = l->listenumerator_;
    l->listenumerator_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListType_qualifier reverseListType_qualifier(ListType_qualifier l)
{
  ListType_qualifier prev = 0;
  ListType_qualifier tmp = 0;
  while (l)
  {
    tmp = l->listtype_qualifier_;
    l->listtype_qualifier_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListIdent reverseListIdent(ListIdent l)
{
  ListIdent prev = 0;
  ListIdent tmp = 0;
  while (l)
  {
    tmp = l->listident_;
    l->listident_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListStm reverseListStm(ListStm l)
{
  ListStm prev = 0;
  ListStm tmp = 0;
  while (l)
  {
    tmp = l->liststm_;
    l->liststm_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}
ListExp reverseListExp(ListExp l)
{
  ListExp prev = 0;
  ListExp tmp = 0;
  while (l)
  {
    tmp = l->listexp_;
    l->listexp_ = prev;
    prev = l;
    l = tmp;
  }
  return prev;
}


#line 965 "Parser.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int c_debug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    _ERROR_ = 258,
    _SYMB_0 = 259,
    _SYMB_1 = 260,
    _SYMB_2 = 261,
    _SYMB_3 = 262,
    _SYMB_4 = 263,
    _SYMB_5 = 264,
    _SYMB_6 = 265,
    _SYMB_7 = 266,
    _SYMB_8 = 267,
    _SYMB_9 = 268,
    _SYMB_10 = 269,
    _SYMB_11 = 270,
    _SYMB_12 = 271,
    _SYMB_13 = 272,
    _SYMB_14 = 273,
    _SYMB_15 = 274,
    _SYMB_16 = 275,
    _SYMB_17 = 276,
    _SYMB_18 = 277,
    _SYMB_19 = 278,
    _SYMB_20 = 279,
    _SYMB_21 = 280,
    _SYMB_22 = 281,
    _SYMB_23 = 282,
    _SYMB_24 = 283,
    _SYMB_25 = 284,
    _SYMB_26 = 285,
    _SYMB_27 = 286,
    _SYMB_28 = 287,
    _SYMB_29 = 288,
    _SYMB_30 = 289,
    _SYMB_31 = 290,
    _SYMB_32 = 291,
    _SYMB_33 = 292,
    _SYMB_34 = 293,
    _SYMB_35 = 294,
    _SYMB_36 = 295,
    _SYMB_37 = 296,
    _SYMB_38 = 297,
    _SYMB_39 = 298,
    _SYMB_40 = 299,
    _SYMB_41 = 300,
    _SYMB_42 = 301,
    _SYMB_43 = 302,
    _SYMB_44 = 303,
    _SYMB_45 = 304,
    _SYMB_46 = 305,
    _SYMB_47 = 306,
    _SYMB_48 = 307,
    _SYMB_49 = 308,
    _SYMB_50 = 309,
    _SYMB_51 = 310,
    _SYMB_52 = 311,
    _SYMB_53 = 312,
    _SYMB_54 = 313,
    _SYMB_55 = 314,
    _SYMB_56 = 315,
    _SYMB_57 = 316,
    _SYMB_58 = 317,
    _SYMB_59 = 318,
    _SYMB_60 = 319,
    _SYMB_61 = 320,
    _SYMB_62 = 321,
    _SYMB_63 = 322,
    _SYMB_64 = 323,
    _SYMB_65 = 324,
    _SYMB_66 = 325,
    _SYMB_67 = 326,
    _SYMB_68 = 327,
    _SYMB_69 = 328,
    _SYMB_70 = 329,
    _SYMB_71 = 330,
    _SYMB_72 = 331,
    _SYMB_73 = 332,
    _SYMB_74 = 333,
    _SYMB_75 = 334,
    _SYMB_76 = 335,
    _SYMB_77 = 336,
    _SYMB_78 = 337,
    _SYMB_79 = 338,
    _SYMB_80 = 339,
    _SYMB_81 = 340,
    _SYMB_82 = 341,
    _SYMB_83 = 342,
    _SYMB_84 = 343,
    _SYMB_85 = 344,
    _SYMB_86 = 345,
    _SYMB_87 = 346,
    _SYMB_88 = 347,
    _SYMB_89 = 348,
    _SYMB_90 = 349,
    _SYMB_91 = 350,
    _SYMB_92 = 351,
    _STRING_ = 352,
    _CHAR_ = 353,
    _INTEGER_ = 354,
    _DOUBLE_ = 355,
    _IDENT_ = 356
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 894 "c.y" /* yacc.c:355  */

  int int_;
  char char_;
  double double_;
  char* string_;
  Program program_;
  ListExternal_declaration listexternal_declaration_;
  External_declaration external_declaration_;
  Function_def function_def_;
  Dec dec_;
  ListDec listdec_;
  ListDeclaration_specifier listdeclaration_specifier_;
  Declaration_specifier declaration_specifier_;
  ListInit_declarator listinit_declarator_;
  Init_declarator init_declarator_;
  Type_specifier type_specifier_;
  Storage_class_specifier storage_class_specifier_;
  Type_qualifier type_qualifier_;
  Struct_or_union_spec struct_or_union_spec_;
  Struct_or_union struct_or_union_;
  ListStruct_dec liststruct_dec_;
  Struct_dec struct_dec_;
  ListSpec_qual listspec_qual_;
  Spec_qual spec_qual_;
  ListStruct_declarator liststruct_declarator_;
  Struct_declarator struct_declarator_;
  Enum_specifier enum_specifier_;
  ListEnumerator listenumerator_;
  Enumerator enumerator_;
  Declarator declarator_;
  Direct_declarator direct_declarator_;
  Pointer pointer_;
  ListType_qualifier listtype_qualifier_;
  Parameter_type parameter_type_;
  Parameter_declarations parameter_declarations_;
  Parameter_declaration parameter_declaration_;
  ListIdent listident_;
  Initializer initializer_;
  Initializers initializers_;
  Type_name type_name_;
  Abstract_declarator abstract_declarator_;
  Dir_abs_dec dir_abs_dec_;
  Stm stm_;
  Labeled_stm labeled_stm_;
  Compound_stm compound_stm_;
  Expression_stm expression_stm_;
  Selection_stm selection_stm_;
  Iter_stm iter_stm_;
  Jump_stm jump_stm_;
  ListStm liststm_;
  Exp exp_;
  Constant constant_;
  Constant_expression constant_expression_;
  Unary_operator unary_operator_;
  ListExp listexp_;
  Assignment_op assignment_op_;


#line 1163 "Parser.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE c_lval;

int c_parse (void);



/* Copy the second part of user declarations.  */

#line 1180 "Parser.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  46
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1734

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  102
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  68
/* YYNRULES -- Number of rules.  */
#define YYNRULES  229
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  367

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   356

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,  1124,  1124,  1126,  1127,  1129,  1130,  1132,  1133,  1134,
    1135,  1137,  1138,  1140,  1141,  1143,  1144,  1146,  1147,  1148,
    1150,  1151,  1153,  1154,  1156,  1157,  1158,  1159,  1160,  1161,
    1162,  1163,  1164,  1165,  1166,  1167,  1169,  1170,  1171,  1172,
    1173,  1175,  1176,  1178,  1179,  1180,  1182,  1183,  1185,  1186,
    1188,  1190,  1191,  1193,  1194,  1196,  1197,  1199,  1200,  1201,
    1203,  1204,  1205,  1207,  1208,  1210,  1211,  1213,  1214,  1216,
    1217,  1218,  1219,  1220,  1221,  1222,  1224,  1225,  1226,  1227,
    1229,  1230,  1232,  1233,  1235,  1236,  1238,  1239,  1240,  1242,
    1243,  1245,  1246,  1247,  1249,  1250,  1252,  1253,  1255,  1256,
    1257,  1259,  1260,  1261,  1262,  1263,  1264,  1265,  1266,  1267,
    1269,  1270,  1271,  1272,  1273,  1274,  1276,  1277,  1278,  1280,
    1281,  1282,  1283,  1285,  1286,  1288,  1289,  1290,  1292,  1293,
    1294,  1295,  1297,  1298,  1299,  1300,  1301,  1303,  1304,  1306,
    1307,  1309,  1310,  1312,  1313,  1315,  1316,  1318,  1319,  1321,
    1322,  1324,  1325,  1327,  1328,  1330,  1331,  1332,  1334,  1335,
    1336,  1337,  1338,  1340,  1341,  1342,  1344,  1345,  1346,  1348,
    1349,  1350,  1351,  1353,  1354,  1356,  1357,  1358,  1359,  1360,
    1361,  1363,  1364,  1365,  1366,  1367,  1368,  1369,  1370,  1372,
    1373,  1374,  1375,  1377,  1378,  1379,  1380,  1381,  1382,  1383,
    1384,  1385,  1386,  1387,  1388,  1389,  1390,  1391,  1392,  1393,
    1395,  1397,  1398,  1399,  1400,  1401,  1402,  1404,  1405,  1407,
    1408,  1409,  1410,  1411,  1412,  1413,  1414,  1415,  1416,  1417
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "_ERROR_", "_SYMB_0", "_SYMB_1",
  "_SYMB_2", "_SYMB_3", "_SYMB_4", "_SYMB_5", "_SYMB_6", "_SYMB_7",
  "_SYMB_8", "_SYMB_9", "_SYMB_10", "_SYMB_11", "_SYMB_12", "_SYMB_13",
  "_SYMB_14", "_SYMB_15", "_SYMB_16", "_SYMB_17", "_SYMB_18", "_SYMB_19",
  "_SYMB_20", "_SYMB_21", "_SYMB_22", "_SYMB_23", "_SYMB_24", "_SYMB_25",
  "_SYMB_26", "_SYMB_27", "_SYMB_28", "_SYMB_29", "_SYMB_30", "_SYMB_31",
  "_SYMB_32", "_SYMB_33", "_SYMB_34", "_SYMB_35", "_SYMB_36", "_SYMB_37",
  "_SYMB_38", "_SYMB_39", "_SYMB_40", "_SYMB_41", "_SYMB_42", "_SYMB_43",
  "_SYMB_44", "_SYMB_45", "_SYMB_46", "_SYMB_47", "_SYMB_48", "_SYMB_49",
  "_SYMB_50", "_SYMB_51", "_SYMB_52", "_SYMB_53", "_SYMB_54", "_SYMB_55",
  "_SYMB_56", "_SYMB_57", "_SYMB_58", "_SYMB_59", "_SYMB_60", "_SYMB_61",
  "_SYMB_62", "_SYMB_63", "_SYMB_64", "_SYMB_65", "_SYMB_66", "_SYMB_67",
  "_SYMB_68", "_SYMB_69", "_SYMB_70", "_SYMB_71", "_SYMB_72", "_SYMB_73",
  "_SYMB_74", "_SYMB_75", "_SYMB_76", "_SYMB_77", "_SYMB_78", "_SYMB_79",
  "_SYMB_80", "_SYMB_81", "_SYMB_82", "_SYMB_83", "_SYMB_84", "_SYMB_85",
  "_SYMB_86", "_SYMB_87", "_SYMB_88", "_SYMB_89", "_SYMB_90", "_SYMB_91",
  "_SYMB_92", "_STRING_", "_CHAR_", "_INTEGER_", "_DOUBLE_", "_IDENT_",
  "$accept", "Program", "ListExternal_declaration", "External_declaration",
  "Function_def", "Dec", "ListDec", "ListDeclaration_specifier",
  "Declaration_specifier", "ListInit_declarator", "Init_declarator",
  "Type_specifier", "Storage_class_specifier", "Type_qualifier",
  "Struct_or_union_spec", "Struct_or_union", "ListStruct_dec",
  "Struct_dec", "ListSpec_qual", "Spec_qual", "ListStruct_declarator",
  "Struct_declarator", "Enum_specifier", "ListEnumerator", "Enumerator",
  "Declarator", "Direct_declarator", "Pointer", "ListType_qualifier",
  "Parameter_type", "Parameter_declarations", "Parameter_declaration",
  "ListIdent", "Initializer", "Initializers", "Type_name",
  "Abstract_declarator", "Dir_abs_dec", "Stm", "Labeled_stm",
  "Compound_stm", "Expression_stm", "Selection_stm", "Iter_stm",
  "Jump_stm", "ListStm", "Exp", "Exp2", "Exp3", "Exp4", "Exp5", "Exp6",
  "Exp7", "Exp8", "Exp9", "Exp10", "Exp11", "Exp12", "Exp13", "Exp14",
  "Exp15", "Exp16", "Exp17", "Constant", "Constant_expression",
  "Unary_operator", "ListExp2", "Assignment_op", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356
};
# endif

#define YYPACT_NINF -207

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-207)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    1395,    -2,    -1,  -207,  -207,  -207,  -207,  -207,     2,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,    92,  -207,  1395,  -207,  -207,    21,
    1653,  -207,  -207,  -207,  -207,     7,  -207,  1506,    48,    10,
      25,   -28,  -207,    34,   -25,   117,  -207,  -207,  -207,   136,
     144,  1472,  -207,   230,   170,   265,  1653,   176,    21,  -207,
    1431,   900,    48,  -207,  -207,  -207,   196,   198,   206,   -25,
    -207,    -2,   716,   176,  -207,  -207,  -207,   214,   230,    12,
     230,   230,  -207,  -207,   808,  -207,  -207,  -207,  -207,  1174,
    1174,  -207,  -207,   235,  1266,   238,   234,   461,   237,   143,
     239,   559,  1296,   240,   241,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,   236,   363,   461,  -207,  -207,  -207,  -207,
    -207,  -207,   244,   111,  -207,  -207,   119,   242,   229,   233,
     245,   173,   162,   181,   183,    36,  -207,   188,    27,  -207,
    -207,  1266,  -207,  -207,   248,  -207,   250,    28,   247,   254,
    -207,   260,  -207,  -207,  -207,  -207,   261,  1266,  -207,   -25,
     270,  -207,   716,  -207,  -207,  -207,  -207,  -207,  1266,   277,
     282,   279,  -207,   284,   134,   283,    66,  1266,  -207,  -207,
    -207,   281,  -207,   461,   224,   591,   303,  1266,  -207,   211,
     808,  -207,  1266,  1266,   461,  -207,   304,  -207,  -207,  -207,
    1266,  1266,  1266,  1266,  1266,  1266,  1266,  1266,  1266,  1266,
    1266,  1266,  1266,  1266,  1266,  1266,  1266,  1266,  1266,  1266,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  1266,   930,  1266,  -207,  -207,   267,   268,  -207,   271,
    1360,  1022,  -207,    20,  -207,   135,  -207,  1621,  -207,  -207,
    -207,  -207,  -207,  -207,   115,  -207,  -207,    12,  1266,  -207,
    1552,   193,  -207,  1266,  -207,   461,  -207,   315,   591,  -207,
      76,  -207,   364,   114,   127,  -207,  -207,  -207,    84,   242,
     229,   233,   245,   173,   162,   162,   181,   181,   181,   181,
     183,   183,    36,    36,  -207,  -207,  -207,  -207,  -207,   369,
     365,    54,  -207,  -207,  -207,  -207,   367,   368,  -207,   370,
     135,  1584,  1052,  -207,  -207,   686,  -207,  -207,  -207,  -207,
    -207,  1266,  1144,   461,  -207,   461,   461,  1266,  1266,  -207,
    -207,  -207,  -207,  -207,  -207,   371,  -207,   372,  -207,  -207,
     128,   461,   132,   320,  -207,  -207,  -207,  -207,  -207,  -207,
     377,  -207,   461,   461,  -207,  -207,  -207
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,    76,    35,    39,    25,    41,    30,     0,    37,
      29,    27,    28,    40,    26,    31,    38,    46,    36,    47,
      32,    24,    42,    69,     0,     2,     3,     5,     6,     0,
      15,    17,    18,    19,    33,     0,    34,     0,    68,     0,
       0,    80,    78,    77,     0,    62,     1,     4,    11,     0,
      20,    22,    16,     0,    45,     0,    13,     0,     0,    10,
       0,     0,    67,    70,    81,    79,    65,     0,    63,     0,
      12,     0,     0,     0,     8,    53,    54,     0,    48,     0,
      51,     0,   123,   119,     0,   212,   211,   213,   214,     0,
       0,   215,   216,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   195,   196,   197,   198,   199,
     200,   201,   202,   203,   204,   205,   206,   207,   208,   191,
     194,   209,   193,   189,     0,   137,   110,   111,   112,   113,
     114,   115,     0,     0,   140,   142,   144,   146,   148,   150,
     152,   154,   157,   162,   165,   168,   172,   174,   180,   188,
     190,     0,    14,     9,    22,    75,    89,    86,     0,    82,
      84,     0,    72,   189,   210,   174,     0,     0,    60,     0,
       0,    21,     0,    23,    91,     7,    44,    49,     0,     0,
      55,    57,    52,     0,    96,     0,     0,     0,   175,   176,
     134,     0,   133,     0,     0,     0,     0,     0,   135,     0,
       0,   178,     0,     0,     0,   121,     0,   138,   120,   124,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     219,   220,   221,   222,   223,   224,   225,   226,   227,   228,
     229,     0,     0,     0,   186,   187,     0,     0,   177,     0,
       0,     0,    87,    98,    88,    99,    73,     0,    74,    71,
      66,    64,    61,    94,     0,    58,    50,     0,     0,    43,
       0,    98,    97,     0,   192,     0,   118,     0,     0,   132,
       0,   136,     0,     0,     0,   116,   122,   139,     0,   145,
     147,   149,   151,   153,   155,   156,   158,   159,   160,   161,
     163,   164,   166,   167,   169,   170,   171,   141,   182,   217,
       0,     0,   184,   185,    90,   106,     0,     0,   102,     0,
     100,     0,     0,    83,    85,     0,    92,    56,    59,   173,
     117,     0,     0,     0,   179,     0,     0,     0,     0,   183,
     181,   107,   101,   103,   108,     0,   104,     0,    93,    95,
       0,     0,     0,   125,   127,   128,   143,   218,   109,   105,
       0,   130,     0,     0,   129,   131,   126
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -207,  -207,   360,  -207,  -207,    46,    23,    75,  -207,   316,
    -207,   -29,  -207,     4,  -207,  -207,   100,  -207,   -73,  -207,
     121,  -207,  -207,   -59,  -207,    15,   -34,    13,   348,   -57,
    -207,   133,   142,  -149,  -207,   192,  -114,  -206,   -95,  -207,
      77,  -178,  -207,  -207,  -207,    93,   -18,   -68,   -60,  -207,
     184,   182,   185,   189,   187,     3,   -21,     0,     1,  -132,
     -61,  -207,  -207,  -207,   -76,  -207,    62,  -207
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    24,    25,    26,    27,    56,    57,   157,    30,    49,
      50,    31,    32,    33,    34,    35,    77,    78,    79,    80,
     179,   180,    36,    67,    68,    37,    38,    39,    43,   316,
     159,   160,   161,   173,   264,   185,   317,   255,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   166,   151,   310,   241
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint16 yytable[] =
{
     165,   164,   194,   158,   174,    62,    41,   182,     1,    44,
     170,   184,     2,     2,    53,    42,    40,   278,   191,   248,
       1,   178,     1,   263,    75,    48,     2,     6,   188,   189,
     250,     1,   251,   165,   164,     2,    63,   242,   250,   243,
     251,   201,     2,   254,    51,    41,    28,   320,     2,    75,
     227,    75,    75,    22,     6,    75,    65,    76,    60,   210,
      61,   244,   245,   246,   247,   320,   186,   340,   228,   229,
     272,   210,    28,   154,    73,    29,    66,   274,   124,   152,
      22,   210,    76,   199,    76,    76,   154,   333,    76,   210,
     165,   260,    46,   337,   181,   304,   305,   306,   276,    23,
     332,    29,   265,    45,   174,    52,   165,   164,    54,   285,
     261,    23,    58,    23,    59,   209,   210,   165,   164,   210,
     325,    23,    23,   326,    69,   335,    58,   184,    74,    23,
      58,    58,   210,   210,   153,   211,   212,   210,   336,   360,
      70,   329,   287,   362,   270,   321,   251,   322,     2,    71,
     175,   165,   165,   165,   165,   165,   165,   165,   165,   165,
     165,   165,   165,   165,   165,   165,   165,   165,   165,   186,
     253,    75,   252,   307,   309,   319,   349,    81,   177,   280,
     330,   183,   186,    55,   283,   284,   219,   220,   221,   222,
     165,   164,   328,   288,   230,   217,   218,   271,   296,   297,
     298,   299,   167,   270,    76,   251,   168,   165,   164,   223,
     224,   169,   165,   225,   226,   281,   210,   206,   207,    62,
     294,   295,   176,   300,   301,   311,   302,   303,   231,   232,
     233,   234,   235,   236,   237,   238,   239,   240,   353,   190,
     354,   355,   192,   193,   196,   204,   347,   195,   214,   197,
     202,   203,   208,   215,    72,   249,   361,   174,   256,   257,
     213,   165,   164,   253,   345,    40,   216,   365,   366,    82,
     309,   258,    55,    83,   259,    84,   165,   356,   262,    85,
       3,   266,   181,   271,     5,     6,    86,   267,   268,     7,
     275,     8,   269,    10,   273,    87,    88,    11,    12,    89,
      90,    14,    15,    91,    92,    17,   277,   279,    19,    20,
      21,    22,   286,   350,   352,     3,     4,    93,    94,     5,
       6,    95,    96,    97,     7,   331,     8,     9,    10,    98,
      99,   100,    11,    12,    13,   101,    14,    15,   102,    16,
      17,   103,    18,    19,    20,    21,    22,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,    82,   312,   313,
      55,   205,   156,    84,   338,   334,   339,    85,   341,   342,
     363,   364,   358,   343,    86,   359,    47,   171,   327,    64,
     324,   314,   282,    87,    88,   290,   289,    89,    90,   291,
     357,    91,    92,   293,   292,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    93,    94,     0,     0,    95,
      96,    97,     0,     0,     0,     0,     0,    98,    99,   100,
       0,     0,     0,   101,     0,     0,   102,     0,     0,   103,
       0,     0,     0,     0,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,    82,     0,     0,    55,     0,
       0,    84,     0,     0,     0,    85,     0,     0,     0,     0,
       0,     0,    86,     0,     0,     0,     0,     0,     0,     0,
       0,    87,    88,     0,     0,    89,    90,     0,     0,    91,
      92,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    93,    94,     0,     0,    95,    96,    97,
       0,     0,     0,     0,     0,    98,    99,   100,     0,     0,
       0,   101,     0,     0,   102,     0,     0,   103,     0,     0,
       0,     0,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   198,     0,     0,     0,     0,     0,    84,
       0,     0,     0,    85,     0,     0,     0,     0,     0,     0,
      86,     0,     0,     0,     0,     0,     0,     0,     0,    87,
      88,     0,     0,    89,    90,    82,     0,    91,    92,     0,
       0,    84,     0,     0,     0,    85,     0,     0,     0,     0,
       0,     0,    86,     0,     0,     0,     0,     0,     0,     0,
       0,    87,    88,     0,     0,    89,    90,     0,     0,    91,
      92,     0,   102,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     163,     0,     0,     0,   102,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   163,   172,   348,     0,    84,     0,     0,     0,
      85,     0,     0,     0,     0,     0,     0,    86,     0,     0,
       0,     0,     0,     0,     0,     0,    87,    88,     0,     0,
      89,    90,     0,   172,    91,    92,    84,     0,     0,     0,
      85,     0,     0,     0,     0,     0,     0,    86,     0,     0,
       0,     0,     0,     0,     0,     0,    87,    88,     0,     0,
      89,    90,     0,     0,    91,    92,     0,     0,     0,   102,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   163,     0,   102,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   163,    84,     0,
       0,     0,    85,     0,     0,     0,     0,     0,     0,    86,
       0,     0,     0,     0,     0,     0,     0,     0,    87,    88,
       0,     0,    89,    90,     0,     0,    91,    92,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     3,     0,
       0,     0,     5,     6,     0,     0,     0,     7,     0,     8,
       0,    10,     0,     0,     0,    11,    12,     0,     0,    14,
      15,   102,     0,    17,     0,     0,    19,    20,    21,    22,
       0,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   163,
      84,     0,     0,   162,    85,     0,     0,     0,     0,     0,
       0,    86,     0,     0,     0,     0,     0,     0,     0,     0,
      87,    88,     0,     0,    89,    90,     0,     0,    91,    92,
      84,   308,     0,     0,    85,     0,     0,     0,     0,     0,
       0,    86,     0,     0,     0,     0,     0,     0,     0,     0,
      87,    88,     0,     0,    89,    90,     0,     0,    91,    92,
       0,     0,     0,   102,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   163,     0,   102,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   163,    84,     0,     0,   318,    85,     0,     0,     0,
       0,     0,     0,    86,     0,     0,     0,     0,     0,     0,
       0,     0,    87,    88,     0,     0,    89,    90,     0,     0,
      91,    92,    84,     0,     0,   346,    85,     0,     0,     0,
       0,     0,     0,    86,     0,     0,     0,     0,     0,     0,
       0,     0,    87,    88,     0,     0,    89,    90,     0,     0,
      91,    92,     0,     0,     0,   102,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   163,     0,   102,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   163,    84,   351,     0,     0,    85,     0,
       0,     0,     0,     0,     0,    86,     0,     0,     0,     0,
       0,     0,     0,     0,    87,    88,     0,     0,    89,    90,
       0,     0,    91,    92,   187,     0,     0,     0,    85,     0,
       0,     0,     0,     0,     0,    86,     0,     0,     0,     0,
       0,     0,     0,     0,    87,    88,     0,     0,    89,    90,
       0,     0,    91,    92,     0,     0,     0,   102,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   163,     0,   102,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   163,    84,     0,     0,     0,
      85,     0,     0,     0,     0,     0,     0,    86,     0,     0,
       0,     0,     0,     0,     0,     0,    87,    88,     0,     0,
      89,    90,     0,     0,    91,    92,   200,     0,     0,     0,
      85,     0,     0,     0,     0,     0,     0,    86,     0,     0,
       0,     0,     0,     0,     0,     0,    87,    88,     0,     0,
      89,    90,     0,     0,    91,    92,     0,     0,     0,   102,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   163,     0,   102,
     250,   315,   251,     0,     2,     0,     0,     0,     0,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   163,     0,     0,
       0,     0,     0,     0,     0,     1,     0,     0,     0,     2,
       3,     4,     0,     0,     5,     6,     0,     0,     0,     7,
       0,     8,     9,    10,     0,     0,     0,    11,    12,    13,
       0,    14,    15,     0,    16,    17,     0,    18,    19,    20,
      21,    22,   155,     0,     0,     3,     4,     0,     0,     5,
       6,     0,     0,     0,     7,     0,     8,     9,    10,     0,
       0,    23,    11,    12,    13,     0,    14,    15,     0,    16,
      17,     0,    18,    19,    20,    21,    22,     0,    72,    55,
       0,     3,     4,     0,     0,     5,     6,     0,     0,     0,
       7,     0,     8,     9,    10,     0,    23,     0,    11,    12,
      13,     0,    14,    15,     0,    16,    17,     0,    18,    19,
      20,    21,    22,    55,     0,     0,     0,     0,     0,     0,
       0,     0,     3,     4,     0,     0,     5,     6,     0,     0,
       0,     7,   156,     8,     9,    10,     0,     0,     0,    11,
      12,    13,     0,    14,    15,     0,    16,    17,     0,    18,
      19,    20,    21,    22,     0,     0,     3,     4,     0,     0,
       5,     6,   270,   315,   251,     7,     2,     8,     9,    10,
       0,     0,     0,    11,    12,    13,     0,    14,    15,     0,
      16,    17,     0,    18,    19,    20,    21,    22,     0,     0,
       0,     0,     0,     0,     0,   344,     0,     0,     0,     0,
       0,     0,     3,     4,     0,     0,     5,     6,     0,     0,
       0,     7,     0,     8,     9,    10,     0,     0,     0,    11,
      12,    13,     0,    14,    15,     0,    16,    17,     0,    18,
      19,    20,    21,    22,     3,     4,   323,     0,     5,     6,
       0,     0,     0,     7,     0,     8,     9,    10,     0,     0,
       0,    11,    12,    13,     0,    14,    15,     0,    16,    17,
       0,    18,    19,    20,    21,    22,     0,     0,     0,     0,
       0,     3,     4,     0,     0,     5,     6,     0,     0,     0,
       7,     0,     8,     9,    10,     0,     0,     0,    11,    12,
      13,     0,    14,    15,     0,    16,    17,     0,    18,    19,
      20,    21,    22,     3,     4,     0,     0,     5,     6,     0,
       0,     0,     7,     0,     8,     9,    10,     0,     0,     0,
      11,    12,    13,     0,    14,    15,     0,    16,    17,     0,
      18,    19,    20,    21,    22
};

static const yytype_int16 yycheck[] =
{
      61,    61,    97,    60,    72,    39,     2,    80,    10,     7,
      69,    84,    14,    14,     7,     2,     1,   195,    94,   151,
      10,     9,    10,   172,    53,     4,    14,    55,    89,    90,
      10,    10,    12,    94,    94,    14,    11,    10,    10,    12,
      12,   102,    14,   157,    29,    41,     0,   253,    14,    78,
      14,    80,    81,    81,    55,    84,    43,    53,    10,     5,
      12,    34,    35,    36,    37,   271,    84,    13,    32,    33,
     184,     5,    26,    58,    51,     0,   101,    11,    55,    56,
      81,     5,    78,   101,    80,    81,    71,    11,    84,     5,
     151,   167,     0,     9,    79,   227,   228,   229,   193,   101,
     278,    26,   178,   101,   172,    30,   167,   167,   101,   204,
     169,   101,    37,   101,    37,     4,     5,   178,   178,     5,
       5,   101,   101,     8,     7,    11,    51,   200,    51,   101,
      55,    56,     5,     5,    57,    16,    17,     5,    11,    11,
       4,   273,   210,    11,    10,    10,    12,    12,    14,     5,
      73,   212,   213,   214,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,   225,   226,   227,   228,   229,   187,
     157,   200,   157,   241,   242,   251,   325,     7,    78,   197,
     275,    81,   200,     7,   202,   203,    24,    25,    26,    27,
     251,   251,   268,   211,     6,    22,    23,   184,   219,   220,
     221,   222,     6,    10,   200,    12,     8,   268,   268,    28,
      29,     5,   273,    30,    31,     4,     5,   124,   125,   253,
     217,   218,     8,   223,   224,   243,   225,   226,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,   333,     4,
     335,   336,     4,     9,   101,     9,   322,    10,    19,    10,
      10,    10,     8,    20,     6,     5,   351,   325,    11,     5,
      18,   322,   322,   250,   321,   250,    21,   362,   363,     4,
     338,    11,     7,     8,    13,    10,   337,   337,     8,    14,
      50,     4,   267,   270,    54,    55,    21,     5,     9,    59,
       9,    61,     8,    63,    11,    30,    31,    67,    68,    34,
      35,    71,    72,    38,    39,    75,    82,     4,    78,    79,
      80,    81,     8,   331,   332,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    10,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,     4,   101,   101,
       7,     8,   101,    10,     5,    11,    11,    14,    11,    11,
      60,     4,    11,    13,    21,    13,    26,    71,   267,    41,
     257,   249,   200,    30,    31,   213,   212,    34,    35,   214,
     338,    38,    39,   216,   215,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    52,    53,    -1,    -1,    56,
      57,    58,    -1,    -1,    -1,    -1,    -1,    64,    65,    66,
      -1,    -1,    -1,    70,    -1,    -1,    73,    -1,    -1,    76,
      -1,    -1,    -1,    -1,    -1,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,     4,    -1,    -1,     7,    -1,
      -1,    10,    -1,    -1,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    -1,    21,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    30,    31,    -1,    -1,    34,    35,    -1,    -1,    38,
      39,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    52,    53,    -1,    -1,    56,    57,    58,
      -1,    -1,    -1,    -1,    -1,    64,    65,    66,    -1,    -1,
      -1,    70,    -1,    -1,    73,    -1,    -1,    76,    -1,    -1,
      -1,    -1,    -1,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,     4,    -1,    -1,    -1,    -1,    -1,    10,
      -1,    -1,    -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,
      21,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    -1,    34,    35,     4,    -1,    38,    39,    -1,
      -1,    10,    -1,    -1,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    -1,    21,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    30,    31,    -1,    -1,    34,    35,    -1,    -1,    38,
      39,    -1,    73,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,    -1,    -1,    -1,    73,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,     7,     8,    -1,    10,    -1,    -1,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    -1,    21,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    -1,
      34,    35,    -1,     7,    38,    39,    10,    -1,    -1,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    -1,    21,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    -1,
      34,    35,    -1,    -1,    38,    39,    -1,    -1,    -1,    73,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,    -1,    73,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,    10,    -1,
      -1,    -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    21,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    30,    31,
      -1,    -1,    34,    35,    -1,    -1,    38,    39,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    -1,
      -1,    -1,    54,    55,    -1,    -1,    -1,    59,    -1,    61,
      -1,    63,    -1,    -1,    -1,    67,    68,    -1,    -1,    71,
      72,    73,    -1,    75,    -1,    -1,    78,    79,    80,    81,
      -1,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
      10,    -1,    -1,    13,    14,    -1,    -1,    -1,    -1,    -1,
      -1,    21,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      30,    31,    -1,    -1,    34,    35,    -1,    -1,    38,    39,
      10,    11,    -1,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      -1,    21,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      30,    31,    -1,    -1,    34,    35,    -1,    -1,    38,    39,
      -1,    -1,    -1,    73,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,    -1,    73,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,    10,    -1,    -1,    13,    14,    -1,    -1,    -1,
      -1,    -1,    -1,    21,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    -1,    34,    35,    -1,    -1,
      38,    39,    10,    -1,    -1,    13,    14,    -1,    -1,    -1,
      -1,    -1,    -1,    21,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    -1,    34,    35,    -1,    -1,
      38,    39,    -1,    -1,    -1,    73,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,    -1,    73,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,    10,    11,    -1,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    -1,    21,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    -1,    34,    35,
      -1,    -1,    38,    39,    10,    -1,    -1,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    -1,    21,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    -1,    34,    35,
      -1,    -1,    38,    39,    -1,    -1,    -1,    73,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,    -1,    73,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,    10,    -1,    -1,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    -1,    21,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    -1,
      34,    35,    -1,    -1,    38,    39,    10,    -1,    -1,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    -1,    21,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    -1,
      34,    35,    -1,    -1,    38,    39,    -1,    -1,    -1,    73,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,    -1,    73,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    10,    -1,    -1,    -1,    14,
      50,    51,    -1,    -1,    54,    55,    -1,    -1,    -1,    59,
      -1,    61,    62,    63,    -1,    -1,    -1,    67,    68,    69,
      -1,    71,    72,    -1,    74,    75,    -1,    77,    78,    79,
      80,    81,    11,    -1,    -1,    50,    51,    -1,    -1,    54,
      55,    -1,    -1,    -1,    59,    -1,    61,    62,    63,    -1,
      -1,   101,    67,    68,    69,    -1,    71,    72,    -1,    74,
      75,    -1,    77,    78,    79,    80,    81,    -1,     6,     7,
      -1,    50,    51,    -1,    -1,    54,    55,    -1,    -1,    -1,
      59,    -1,    61,    62,    63,    -1,   101,    -1,    67,    68,
      69,    -1,    71,    72,    -1,    74,    75,    -1,    77,    78,
      79,    80,    81,     7,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    50,    51,    -1,    -1,    54,    55,    -1,    -1,
      -1,    59,   101,    61,    62,    63,    -1,    -1,    -1,    67,
      68,    69,    -1,    71,    72,    -1,    74,    75,    -1,    77,
      78,    79,    80,    81,    -1,    -1,    50,    51,    -1,    -1,
      54,    55,    10,    11,    12,    59,    14,    61,    62,    63,
      -1,    -1,    -1,    67,    68,    69,    -1,    71,    72,    -1,
      74,    75,    -1,    77,    78,    79,    80,    81,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    11,    -1,    -1,    -1,    -1,
      -1,    -1,    50,    51,    -1,    -1,    54,    55,    -1,    -1,
      -1,    59,    -1,    61,    62,    63,    -1,    -1,    -1,    67,
      68,    69,    -1,    71,    72,    -1,    74,    75,    -1,    77,
      78,    79,    80,    81,    50,    51,    15,    -1,    54,    55,
      -1,    -1,    -1,    59,    -1,    61,    62,    63,    -1,    -1,
      -1,    67,    68,    69,    -1,    71,    72,    -1,    74,    75,
      -1,    77,    78,    79,    80,    81,    -1,    -1,    -1,    -1,
      -1,    50,    51,    -1,    -1,    54,    55,    -1,    -1,    -1,
      59,    -1,    61,    62,    63,    -1,    -1,    -1,    67,    68,
      69,    -1,    71,    72,    -1,    74,    75,    -1,    77,    78,
      79,    80,    81,    50,    51,    -1,    -1,    54,    55,    -1,
      -1,    -1,    59,    -1,    61,    62,    63,    -1,    -1,    -1,
      67,    68,    69,    -1,    71,    72,    -1,    74,    75,    -1,
      77,    78,    79,    80,    81
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    10,    14,    50,    51,    54,    55,    59,    61,    62,
      63,    67,    68,    69,    71,    72,    74,    75,    77,    78,
      79,    80,    81,   101,   103,   104,   105,   106,   107,   109,
     110,   113,   114,   115,   116,   117,   124,   127,   128,   129,
     127,   115,   129,   130,     7,   101,     0,   104,     4,   111,
     112,   127,   109,     7,   101,     7,   107,   108,   109,   142,
      10,    12,   128,    11,   130,   129,   101,   125,   126,     7,
       4,     5,     6,   108,   142,   113,   115,   118,   119,   120,
     121,     7,     4,     8,    10,    14,    21,    30,    31,    34,
      35,    38,    39,    52,    53,    56,    57,    58,    64,    65,
      66,    70,    73,    76,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   108,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   167,   108,   142,   127,    11,   101,   109,   131,   132,
     133,   134,    13,   101,   150,   162,   166,     6,     8,     5,
     125,   111,     7,   135,   149,   142,     8,   118,     9,   122,
     123,   127,   120,   118,   120,   137,   148,    10,   162,   162,
       4,   166,     4,     9,   140,    10,   101,    10,     4,   148,
      10,   162,    10,    10,     9,     8,   147,   147,     8,     4,
       5,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    14,    32,    33,
       6,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,   169,    10,    12,    34,    35,    36,    37,   161,     5,
      10,    12,   127,   129,   138,   139,    11,     5,    11,    13,
     166,   125,     8,   135,   136,   166,     4,     5,     9,     8,
      10,   129,   138,    11,    11,     9,   140,    82,   143,     4,
     148,     4,   137,   148,   148,   140,     8,   149,   148,   152,
     153,   154,   155,   156,   157,   157,   158,   158,   158,   158,
     159,   159,   160,   160,   161,   161,   161,   149,    11,   149,
     168,   148,   101,   101,   134,    11,   131,   138,    13,   166,
     139,    10,    12,    15,   133,     5,     8,   122,   166,   161,
     140,    10,   143,    11,    11,    11,    11,     9,     5,    11,
      13,    11,    11,    13,    11,   131,    13,   166,     8,   135,
     148,    11,   148,   140,   140,   140,   150,   168,    11,    13,
      11,   140,    11,    60,     4,   140,   140
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   102,   103,   104,   104,   105,   105,   106,   106,   106,
     106,   107,   107,   108,   108,   109,   109,   110,   110,   110,
     111,   111,   112,   112,   113,   113,   113,   113,   113,   113,
     113,   113,   113,   113,   113,   113,   114,   114,   114,   114,
     114,   115,   115,   116,   116,   116,   117,   117,   118,   118,
     119,   120,   120,   121,   121,   122,   122,   123,   123,   123,
     124,   124,   124,   125,   125,   126,   126,   127,   127,   128,
     128,   128,   128,   128,   128,   128,   129,   129,   129,   129,
     130,   130,   131,   131,   132,   132,   133,   133,   133,   134,
     134,   135,   135,   135,   136,   136,   137,   137,   138,   138,
     138,   139,   139,   139,   139,   139,   139,   139,   139,   139,
     140,   140,   140,   140,   140,   140,   141,   141,   141,   142,
     142,   142,   142,   143,   143,   144,   144,   144,   145,   145,
     145,   145,   146,   146,   146,   146,   146,   147,   147,   148,
     148,   149,   149,   150,   150,   151,   151,   152,   152,   153,
     153,   154,   154,   155,   155,   156,   156,   156,   157,   157,
     157,   157,   157,   158,   158,   158,   159,   159,   159,   160,
     160,   160,   160,   161,   161,   162,   162,   162,   162,   162,
     162,   163,   163,   163,   163,   163,   163,   163,   163,   164,
     164,   164,   164,   165,   165,   165,   165,   165,   165,   165,
     165,   165,   165,   165,   165,   165,   165,   165,   165,   165,
     166,   167,   167,   167,   167,   167,   167,   168,   168,   169,
     169,   169,   169,   169,   169,   169,   169,   169,   169,   169
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     2,     1,     1,     4,     3,     3,
       2,     2,     3,     1,     2,     1,     2,     1,     1,     1,
       1,     3,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     5,     4,     2,     1,     1,     1,     2,
       3,     1,     2,     1,     1,     1,     3,     1,     2,     3,
       4,     5,     2,     1,     3,     1,     3,     2,     1,     1,
       3,     4,     3,     4,     4,     3,     1,     2,     2,     3,
       1,     2,     1,     3,     1,     3,     1,     2,     2,     1,
       3,     1,     3,     4,     1,     3,     1,     2,     1,     1,
       2,     3,     2,     3,     3,     4,     2,     3,     3,     4,
       1,     1,     1,     1,     1,     1,     3,     4,     3,     2,
       3,     3,     4,     1,     2,     5,     7,     5,     5,     7,
       6,     7,     3,     2,     2,     2,     3,     1,     2,     3,
       1,     3,     1,     5,     1,     3,     1,     3,     1,     3,
       1,     3,     1,     3,     1,     3,     3,     1,     3,     3,
       3,     3,     1,     3,     3,     1,     3,     3,     1,     3,
       3,     3,     1,     4,     1,     2,     2,     2,     2,     4,
       1,     4,     3,     4,     3,     3,     2,     2,     1,     1,
       1,     1,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 1124 "c.y" /* yacc.c:1646  */
    { (yyval.program_) = make_Progr((yyvsp[0].listexternal_declaration_)); YY_RESULT_Program_= (yyval.program_); }
#line 2828 "Parser.c" /* yacc.c:1646  */
    break;

  case 3:
#line 1126 "c.y" /* yacc.c:1646  */
    { (yyval.listexternal_declaration_) = make_ListExternal_declaration((yyvsp[0].external_declaration_), 0);  }
#line 2834 "Parser.c" /* yacc.c:1646  */
    break;

  case 4:
#line 1127 "c.y" /* yacc.c:1646  */
    { (yyval.listexternal_declaration_) = make_ListExternal_declaration((yyvsp[-1].external_declaration_), (yyvsp[0].listexternal_declaration_));  }
#line 2840 "Parser.c" /* yacc.c:1646  */
    break;

  case 5:
#line 1129 "c.y" /* yacc.c:1646  */
    { (yyval.external_declaration_) = make_Afunc((yyvsp[0].function_def_));  }
#line 2846 "Parser.c" /* yacc.c:1646  */
    break;

  case 6:
#line 1130 "c.y" /* yacc.c:1646  */
    { (yyval.external_declaration_) = make_Global((yyvsp[0].dec_));  }
#line 2852 "Parser.c" /* yacc.c:1646  */
    break;

  case 7:
#line 1132 "c.y" /* yacc.c:1646  */
    { (yyval.function_def_) = make_OldFunc((yyvsp[-3].listdeclaration_specifier_), (yyvsp[-2].declarator_), (yyvsp[-1].listdec_), (yyvsp[0].compound_stm_));  }
#line 2858 "Parser.c" /* yacc.c:1646  */
    break;

  case 8:
#line 1133 "c.y" /* yacc.c:1646  */
    { (yyval.function_def_) = make_NewFunc((yyvsp[-2].listdeclaration_specifier_), (yyvsp[-1].declarator_), (yyvsp[0].compound_stm_));  }
#line 2864 "Parser.c" /* yacc.c:1646  */
    break;

  case 9:
#line 1134 "c.y" /* yacc.c:1646  */
    { (yyval.function_def_) = make_OldFuncInt((yyvsp[-2].declarator_), (yyvsp[-1].listdec_), (yyvsp[0].compound_stm_));  }
#line 2870 "Parser.c" /* yacc.c:1646  */
    break;

  case 10:
#line 1135 "c.y" /* yacc.c:1646  */
    { (yyval.function_def_) = make_NewFuncInt((yyvsp[-1].declarator_), (yyvsp[0].compound_stm_));  }
#line 2876 "Parser.c" /* yacc.c:1646  */
    break;

  case 11:
#line 1137 "c.y" /* yacc.c:1646  */
    { (yyval.dec_) = make_NoDeclarator((yyvsp[-1].listdeclaration_specifier_));  }
#line 2882 "Parser.c" /* yacc.c:1646  */
    break;

  case 12:
#line 1138 "c.y" /* yacc.c:1646  */
    { (yyval.dec_) = make_Declarators((yyvsp[-2].listdeclaration_specifier_), (yyvsp[-1].listinit_declarator_));  }
#line 2888 "Parser.c" /* yacc.c:1646  */
    break;

  case 13:
#line 1140 "c.y" /* yacc.c:1646  */
    { (yyval.listdec_) = make_ListDec((yyvsp[0].dec_), 0);  }
#line 2894 "Parser.c" /* yacc.c:1646  */
    break;

  case 14:
#line 1141 "c.y" /* yacc.c:1646  */
    { (yyval.listdec_) = make_ListDec((yyvsp[-1].dec_), (yyvsp[0].listdec_));  }
#line 2900 "Parser.c" /* yacc.c:1646  */
    break;

  case 15:
#line 1143 "c.y" /* yacc.c:1646  */
    { (yyval.listdeclaration_specifier_) = make_ListDeclaration_specifier((yyvsp[0].declaration_specifier_), 0);  }
#line 2906 "Parser.c" /* yacc.c:1646  */
    break;

  case 16:
#line 1144 "c.y" /* yacc.c:1646  */
    { (yyval.listdeclaration_specifier_) = make_ListDeclaration_specifier((yyvsp[-1].declaration_specifier_), (yyvsp[0].listdeclaration_specifier_));  }
#line 2912 "Parser.c" /* yacc.c:1646  */
    break;

  case 17:
#line 1146 "c.y" /* yacc.c:1646  */
    { (yyval.declaration_specifier_) = make_Type((yyvsp[0].type_specifier_));  }
#line 2918 "Parser.c" /* yacc.c:1646  */
    break;

  case 18:
#line 1147 "c.y" /* yacc.c:1646  */
    { (yyval.declaration_specifier_) = make_Storage((yyvsp[0].storage_class_specifier_));  }
#line 2924 "Parser.c" /* yacc.c:1646  */
    break;

  case 19:
#line 1148 "c.y" /* yacc.c:1646  */
    { (yyval.declaration_specifier_) = make_SpecProp((yyvsp[0].type_qualifier_));  }
#line 2930 "Parser.c" /* yacc.c:1646  */
    break;

  case 20:
#line 1150 "c.y" /* yacc.c:1646  */
    { (yyval.listinit_declarator_) = make_ListInit_declarator((yyvsp[0].init_declarator_), 0);  }
#line 2936 "Parser.c" /* yacc.c:1646  */
    break;

  case 21:
#line 1151 "c.y" /* yacc.c:1646  */
    { (yyval.listinit_declarator_) = make_ListInit_declarator((yyvsp[-2].init_declarator_), (yyvsp[0].listinit_declarator_));  }
#line 2942 "Parser.c" /* yacc.c:1646  */
    break;

  case 22:
#line 1153 "c.y" /* yacc.c:1646  */
    { (yyval.init_declarator_) = make_OnlyDecl((yyvsp[0].declarator_));  }
#line 2948 "Parser.c" /* yacc.c:1646  */
    break;

  case 23:
#line 1154 "c.y" /* yacc.c:1646  */
    { (yyval.init_declarator_) = make_InitDecl((yyvsp[-2].declarator_), (yyvsp[0].initializer_));  }
#line 2954 "Parser.c" /* yacc.c:1646  */
    break;

  case 24:
#line 1156 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tvoid();  }
#line 2960 "Parser.c" /* yacc.c:1646  */
    break;

  case 25:
#line 1157 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tchar();  }
#line 2966 "Parser.c" /* yacc.c:1646  */
    break;

  case 26:
#line 1158 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tshort();  }
#line 2972 "Parser.c" /* yacc.c:1646  */
    break;

  case 27:
#line 1159 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tint();  }
#line 2978 "Parser.c" /* yacc.c:1646  */
    break;

  case 28:
#line 1160 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tlong();  }
#line 2984 "Parser.c" /* yacc.c:1646  */
    break;

  case 29:
#line 1161 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tfloat();  }
#line 2990 "Parser.c" /* yacc.c:1646  */
    break;

  case 30:
#line 1162 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tdouble();  }
#line 2996 "Parser.c" /* yacc.c:1646  */
    break;

  case 31:
#line 1163 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tsigned();  }
#line 3002 "Parser.c" /* yacc.c:1646  */
    break;

  case 32:
#line 1164 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tunsigned();  }
#line 3008 "Parser.c" /* yacc.c:1646  */
    break;

  case 33:
#line 1165 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tstruct((yyvsp[0].struct_or_union_spec_));  }
#line 3014 "Parser.c" /* yacc.c:1646  */
    break;

  case 34:
#line 1166 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tenum((yyvsp[0].enum_specifier_));  }
#line 3020 "Parser.c" /* yacc.c:1646  */
    break;

  case 35:
#line 1167 "c.y" /* yacc.c:1646  */
    { (yyval.type_specifier_) = make_Tname();  }
#line 3026 "Parser.c" /* yacc.c:1646  */
    break;

  case 36:
#line 1169 "c.y" /* yacc.c:1646  */
    { (yyval.storage_class_specifier_) = make_MyType();  }
#line 3032 "Parser.c" /* yacc.c:1646  */
    break;

  case 37:
#line 1170 "c.y" /* yacc.c:1646  */
    { (yyval.storage_class_specifier_) = make_GlobalPrograms();  }
#line 3038 "Parser.c" /* yacc.c:1646  */
    break;

  case 38:
#line 1171 "c.y" /* yacc.c:1646  */
    { (yyval.storage_class_specifier_) = make_LocalProgram();  }
#line 3044 "Parser.c" /* yacc.c:1646  */
    break;

  case 39:
#line 1172 "c.y" /* yacc.c:1646  */
    { (yyval.storage_class_specifier_) = make_LocalBlock();  }
#line 3050 "Parser.c" /* yacc.c:1646  */
    break;

  case 40:
#line 1173 "c.y" /* yacc.c:1646  */
    { (yyval.storage_class_specifier_) = make_LocalReg();  }
#line 3056 "Parser.c" /* yacc.c:1646  */
    break;

  case 41:
#line 1175 "c.y" /* yacc.c:1646  */
    { (yyval.type_qualifier_) = make_Const();  }
#line 3062 "Parser.c" /* yacc.c:1646  */
    break;

  case 42:
#line 1176 "c.y" /* yacc.c:1646  */
    { (yyval.type_qualifier_) = make_NoOptim();  }
#line 3068 "Parser.c" /* yacc.c:1646  */
    break;

  case 43:
#line 1178 "c.y" /* yacc.c:1646  */
    { (yyval.struct_or_union_spec_) = make_Tag((yyvsp[-4].struct_or_union_), (yyvsp[-3].string_), (yyvsp[-1].liststruct_dec_));  }
#line 3074 "Parser.c" /* yacc.c:1646  */
    break;

  case 44:
#line 1179 "c.y" /* yacc.c:1646  */
    { (yyval.struct_or_union_spec_) = make_Unique((yyvsp[-3].struct_or_union_), (yyvsp[-1].liststruct_dec_));  }
#line 3080 "Parser.c" /* yacc.c:1646  */
    break;

  case 45:
#line 1180 "c.y" /* yacc.c:1646  */
    { (yyval.struct_or_union_spec_) = make_TagType((yyvsp[-1].struct_or_union_), (yyvsp[0].string_));  }
#line 3086 "Parser.c" /* yacc.c:1646  */
    break;

  case 46:
#line 1182 "c.y" /* yacc.c:1646  */
    { (yyval.struct_or_union_) = make_Struct();  }
#line 3092 "Parser.c" /* yacc.c:1646  */
    break;

  case 47:
#line 1183 "c.y" /* yacc.c:1646  */
    { (yyval.struct_or_union_) = make_Union();  }
#line 3098 "Parser.c" /* yacc.c:1646  */
    break;

  case 48:
#line 1185 "c.y" /* yacc.c:1646  */
    { (yyval.liststruct_dec_) = make_ListStruct_dec((yyvsp[0].struct_dec_), 0);  }
#line 3104 "Parser.c" /* yacc.c:1646  */
    break;

  case 49:
#line 1186 "c.y" /* yacc.c:1646  */
    { (yyval.liststruct_dec_) = make_ListStruct_dec((yyvsp[-1].struct_dec_), (yyvsp[0].liststruct_dec_));  }
#line 3110 "Parser.c" /* yacc.c:1646  */
    break;

  case 50:
#line 1188 "c.y" /* yacc.c:1646  */
    { (yyval.struct_dec_) = make_Structen((yyvsp[-2].listspec_qual_), (yyvsp[-1].liststruct_declarator_));  }
#line 3116 "Parser.c" /* yacc.c:1646  */
    break;

  case 51:
#line 1190 "c.y" /* yacc.c:1646  */
    { (yyval.listspec_qual_) = make_ListSpec_qual((yyvsp[0].spec_qual_), 0);  }
#line 3122 "Parser.c" /* yacc.c:1646  */
    break;

  case 52:
#line 1191 "c.y" /* yacc.c:1646  */
    { (yyval.listspec_qual_) = make_ListSpec_qual((yyvsp[-1].spec_qual_), (yyvsp[0].listspec_qual_));  }
#line 3128 "Parser.c" /* yacc.c:1646  */
    break;

  case 53:
#line 1193 "c.y" /* yacc.c:1646  */
    { (yyval.spec_qual_) = make_TypeSpec((yyvsp[0].type_specifier_));  }
#line 3134 "Parser.c" /* yacc.c:1646  */
    break;

  case 54:
#line 1194 "c.y" /* yacc.c:1646  */
    { (yyval.spec_qual_) = make_QualSpec((yyvsp[0].type_qualifier_));  }
#line 3140 "Parser.c" /* yacc.c:1646  */
    break;

  case 55:
#line 1196 "c.y" /* yacc.c:1646  */
    { (yyval.liststruct_declarator_) = make_ListStruct_declarator((yyvsp[0].struct_declarator_), 0);  }
#line 3146 "Parser.c" /* yacc.c:1646  */
    break;

  case 56:
#line 1197 "c.y" /* yacc.c:1646  */
    { (yyval.liststruct_declarator_) = make_ListStruct_declarator((yyvsp[-2].struct_declarator_), (yyvsp[0].liststruct_declarator_));  }
#line 3152 "Parser.c" /* yacc.c:1646  */
    break;

  case 57:
#line 1199 "c.y" /* yacc.c:1646  */
    { (yyval.struct_declarator_) = make_Decl((yyvsp[0].declarator_));  }
#line 3158 "Parser.c" /* yacc.c:1646  */
    break;

  case 58:
#line 1200 "c.y" /* yacc.c:1646  */
    { (yyval.struct_declarator_) = make_Field((yyvsp[0].constant_expression_));  }
#line 3164 "Parser.c" /* yacc.c:1646  */
    break;

  case 59:
#line 1201 "c.y" /* yacc.c:1646  */
    { (yyval.struct_declarator_) = make_DecField((yyvsp[-2].declarator_), (yyvsp[0].constant_expression_));  }
#line 3170 "Parser.c" /* yacc.c:1646  */
    break;

  case 60:
#line 1203 "c.y" /* yacc.c:1646  */
    { (yyval.enum_specifier_) = make_EnumDec((yyvsp[-1].listenumerator_));  }
#line 3176 "Parser.c" /* yacc.c:1646  */
    break;

  case 61:
#line 1204 "c.y" /* yacc.c:1646  */
    { (yyval.enum_specifier_) = make_EnumName((yyvsp[-3].string_), (yyvsp[-1].listenumerator_));  }
#line 3182 "Parser.c" /* yacc.c:1646  */
    break;

  case 62:
#line 1205 "c.y" /* yacc.c:1646  */
    { (yyval.enum_specifier_) = make_EnumVar((yyvsp[0].string_));  }
#line 3188 "Parser.c" /* yacc.c:1646  */
    break;

  case 63:
#line 1207 "c.y" /* yacc.c:1646  */
    { (yyval.listenumerator_) = make_ListEnumerator((yyvsp[0].enumerator_), 0);  }
#line 3194 "Parser.c" /* yacc.c:1646  */
    break;

  case 64:
#line 1208 "c.y" /* yacc.c:1646  */
    { (yyval.listenumerator_) = make_ListEnumerator((yyvsp[-2].enumerator_), (yyvsp[0].listenumerator_));  }
#line 3200 "Parser.c" /* yacc.c:1646  */
    break;

  case 65:
#line 1210 "c.y" /* yacc.c:1646  */
    { (yyval.enumerator_) = make_Plain((yyvsp[0].string_));  }
#line 3206 "Parser.c" /* yacc.c:1646  */
    break;

  case 66:
#line 1211 "c.y" /* yacc.c:1646  */
    { (yyval.enumerator_) = make_EnumInit((yyvsp[-2].string_), (yyvsp[0].constant_expression_));  }
#line 3212 "Parser.c" /* yacc.c:1646  */
    break;

  case 67:
#line 1213 "c.y" /* yacc.c:1646  */
    { (yyval.declarator_) = make_BeginPointer((yyvsp[-1].pointer_), (yyvsp[0].direct_declarator_));  }
#line 3218 "Parser.c" /* yacc.c:1646  */
    break;

  case 68:
#line 1214 "c.y" /* yacc.c:1646  */
    { (yyval.declarator_) = make_NoPointer((yyvsp[0].direct_declarator_));  }
#line 3224 "Parser.c" /* yacc.c:1646  */
    break;

  case 69:
#line 1216 "c.y" /* yacc.c:1646  */
    { (yyval.direct_declarator_) = make_Name((yyvsp[0].string_));  }
#line 3230 "Parser.c" /* yacc.c:1646  */
    break;

  case 70:
#line 1217 "c.y" /* yacc.c:1646  */
    { (yyval.direct_declarator_) = make_ParenDecl((yyvsp[-1].declarator_));  }
#line 3236 "Parser.c" /* yacc.c:1646  */
    break;

  case 71:
#line 1218 "c.y" /* yacc.c:1646  */
    { (yyval.direct_declarator_) = make_InnitArray((yyvsp[-3].direct_declarator_), (yyvsp[-1].constant_expression_));  }
#line 3242 "Parser.c" /* yacc.c:1646  */
    break;

  case 72:
#line 1219 "c.y" /* yacc.c:1646  */
    { (yyval.direct_declarator_) = make_Incomplete((yyvsp[-2].direct_declarator_));  }
#line 3248 "Parser.c" /* yacc.c:1646  */
    break;

  case 73:
#line 1220 "c.y" /* yacc.c:1646  */
    { (yyval.direct_declarator_) = make_NewFuncDec((yyvsp[-3].direct_declarator_), (yyvsp[-1].parameter_type_));  }
#line 3254 "Parser.c" /* yacc.c:1646  */
    break;

  case 74:
#line 1221 "c.y" /* yacc.c:1646  */
    { (yyval.direct_declarator_) = make_OldFuncDef((yyvsp[-3].direct_declarator_), (yyvsp[-1].listident_));  }
#line 3260 "Parser.c" /* yacc.c:1646  */
    break;

  case 75:
#line 1222 "c.y" /* yacc.c:1646  */
    { (yyval.direct_declarator_) = make_OldFuncDec((yyvsp[-2].direct_declarator_));  }
#line 3266 "Parser.c" /* yacc.c:1646  */
    break;

  case 76:
#line 1224 "c.y" /* yacc.c:1646  */
    { (yyval.pointer_) = make_Point();  }
#line 3272 "Parser.c" /* yacc.c:1646  */
    break;

  case 77:
#line 1225 "c.y" /* yacc.c:1646  */
    { (yyval.pointer_) = make_PointQual((yyvsp[0].listtype_qualifier_));  }
#line 3278 "Parser.c" /* yacc.c:1646  */
    break;

  case 78:
#line 1226 "c.y" /* yacc.c:1646  */
    { (yyval.pointer_) = make_PointPoint((yyvsp[0].pointer_));  }
#line 3284 "Parser.c" /* yacc.c:1646  */
    break;

  case 79:
#line 1227 "c.y" /* yacc.c:1646  */
    { (yyval.pointer_) = make_PointQualPoint((yyvsp[-1].listtype_qualifier_), (yyvsp[0].pointer_));  }
#line 3290 "Parser.c" /* yacc.c:1646  */
    break;

  case 80:
#line 1229 "c.y" /* yacc.c:1646  */
    { (yyval.listtype_qualifier_) = make_ListType_qualifier((yyvsp[0].type_qualifier_), 0);  }
#line 3296 "Parser.c" /* yacc.c:1646  */
    break;

  case 81:
#line 1230 "c.y" /* yacc.c:1646  */
    { (yyval.listtype_qualifier_) = make_ListType_qualifier((yyvsp[-1].type_qualifier_), (yyvsp[0].listtype_qualifier_));  }
#line 3302 "Parser.c" /* yacc.c:1646  */
    break;

  case 82:
#line 1232 "c.y" /* yacc.c:1646  */
    { (yyval.parameter_type_) = make_AllSpec((yyvsp[0].parameter_declarations_));  }
#line 3308 "Parser.c" /* yacc.c:1646  */
    break;

  case 83:
#line 1233 "c.y" /* yacc.c:1646  */
    { (yyval.parameter_type_) = make_More((yyvsp[-2].parameter_declarations_));  }
#line 3314 "Parser.c" /* yacc.c:1646  */
    break;

  case 84:
#line 1235 "c.y" /* yacc.c:1646  */
    { (yyval.parameter_declarations_) = make_ParamDec((yyvsp[0].parameter_declaration_));  }
#line 3320 "Parser.c" /* yacc.c:1646  */
    break;

  case 85:
#line 1236 "c.y" /* yacc.c:1646  */
    { (yyval.parameter_declarations_) = make_MoreParamDec((yyvsp[-2].parameter_declarations_), (yyvsp[0].parameter_declaration_));  }
#line 3326 "Parser.c" /* yacc.c:1646  */
    break;

  case 86:
#line 1238 "c.y" /* yacc.c:1646  */
    { (yyval.parameter_declaration_) = make_OnlyType((yyvsp[0].listdeclaration_specifier_));  }
#line 3332 "Parser.c" /* yacc.c:1646  */
    break;

  case 87:
#line 1239 "c.y" /* yacc.c:1646  */
    { (yyval.parameter_declaration_) = make_TypeAndParam((yyvsp[-1].listdeclaration_specifier_), (yyvsp[0].declarator_));  }
#line 3338 "Parser.c" /* yacc.c:1646  */
    break;

  case 88:
#line 1240 "c.y" /* yacc.c:1646  */
    { (yyval.parameter_declaration_) = make_Abstract((yyvsp[-1].listdeclaration_specifier_), (yyvsp[0].abstract_declarator_));  }
#line 3344 "Parser.c" /* yacc.c:1646  */
    break;

  case 89:
#line 1242 "c.y" /* yacc.c:1646  */
    { (yyval.listident_) = make_ListIdent((yyvsp[0].string_), 0);  }
#line 3350 "Parser.c" /* yacc.c:1646  */
    break;

  case 90:
#line 1243 "c.y" /* yacc.c:1646  */
    { (yyval.listident_) = make_ListIdent((yyvsp[-2].string_), (yyvsp[0].listident_));  }
#line 3356 "Parser.c" /* yacc.c:1646  */
    break;

  case 91:
#line 1245 "c.y" /* yacc.c:1646  */
    { (yyval.initializer_) = make_InitExpr((yyvsp[0].exp_));  }
#line 3362 "Parser.c" /* yacc.c:1646  */
    break;

  case 92:
#line 1246 "c.y" /* yacc.c:1646  */
    { (yyval.initializer_) = make_InitListOne((yyvsp[-1].initializers_));  }
#line 3368 "Parser.c" /* yacc.c:1646  */
    break;

  case 93:
#line 1247 "c.y" /* yacc.c:1646  */
    { (yyval.initializer_) = make_InitListTwo((yyvsp[-2].initializers_));  }
#line 3374 "Parser.c" /* yacc.c:1646  */
    break;

  case 94:
#line 1249 "c.y" /* yacc.c:1646  */
    { (yyval.initializers_) = make_AnInit((yyvsp[0].initializer_));  }
#line 3380 "Parser.c" /* yacc.c:1646  */
    break;

  case 95:
#line 1250 "c.y" /* yacc.c:1646  */
    { (yyval.initializers_) = make_MoreInit((yyvsp[-2].initializers_), (yyvsp[0].initializer_));  }
#line 3386 "Parser.c" /* yacc.c:1646  */
    break;

  case 96:
#line 1252 "c.y" /* yacc.c:1646  */
    { (yyval.type_name_) = make_PlainType((yyvsp[0].listspec_qual_));  }
#line 3392 "Parser.c" /* yacc.c:1646  */
    break;

  case 97:
#line 1253 "c.y" /* yacc.c:1646  */
    { (yyval.type_name_) = make_ExtendedType((yyvsp[-1].listspec_qual_), (yyvsp[0].abstract_declarator_));  }
#line 3398 "Parser.c" /* yacc.c:1646  */
    break;

  case 98:
#line 1255 "c.y" /* yacc.c:1646  */
    { (yyval.abstract_declarator_) = make_PointerStart((yyvsp[0].pointer_));  }
#line 3404 "Parser.c" /* yacc.c:1646  */
    break;

  case 99:
#line 1256 "c.y" /* yacc.c:1646  */
    { (yyval.abstract_declarator_) = make_Advanced((yyvsp[0].dir_abs_dec_));  }
#line 3410 "Parser.c" /* yacc.c:1646  */
    break;

  case 100:
#line 1257 "c.y" /* yacc.c:1646  */
    { (yyval.abstract_declarator_) = make_PointAdvanced((yyvsp[-1].pointer_), (yyvsp[0].dir_abs_dec_));  }
#line 3416 "Parser.c" /* yacc.c:1646  */
    break;

  case 101:
#line 1259 "c.y" /* yacc.c:1646  */
    { (yyval.dir_abs_dec_) = make_WithinParentes((yyvsp[-1].abstract_declarator_));  }
#line 3422 "Parser.c" /* yacc.c:1646  */
    break;

  case 102:
#line 1260 "c.y" /* yacc.c:1646  */
    { (yyval.dir_abs_dec_) = make_Array();  }
#line 3428 "Parser.c" /* yacc.c:1646  */
    break;

  case 103:
#line 1261 "c.y" /* yacc.c:1646  */
    { (yyval.dir_abs_dec_) = make_InitiatedArray((yyvsp[-1].constant_expression_));  }
#line 3434 "Parser.c" /* yacc.c:1646  */
    break;

  case 104:
#line 1262 "c.y" /* yacc.c:1646  */
    { (yyval.dir_abs_dec_) = make_UnInitiated((yyvsp[-2].dir_abs_dec_));  }
#line 3440 "Parser.c" /* yacc.c:1646  */
    break;

  case 105:
#line 1263 "c.y" /* yacc.c:1646  */
    { (yyval.dir_abs_dec_) = make_Initiated((yyvsp[-3].dir_abs_dec_), (yyvsp[-1].constant_expression_));  }
#line 3446 "Parser.c" /* yacc.c:1646  */
    break;

  case 106:
#line 1264 "c.y" /* yacc.c:1646  */
    { (yyval.dir_abs_dec_) = make_OldFunction();  }
#line 3452 "Parser.c" /* yacc.c:1646  */
    break;

  case 107:
#line 1265 "c.y" /* yacc.c:1646  */
    { (yyval.dir_abs_dec_) = make_NewFunction((yyvsp[-1].parameter_type_));  }
#line 3458 "Parser.c" /* yacc.c:1646  */
    break;

  case 108:
#line 1266 "c.y" /* yacc.c:1646  */
    { (yyval.dir_abs_dec_) = make_OldFuncExpr((yyvsp[-2].dir_abs_dec_));  }
#line 3464 "Parser.c" /* yacc.c:1646  */
    break;

  case 109:
#line 1267 "c.y" /* yacc.c:1646  */
    { (yyval.dir_abs_dec_) = make_NewFuncExpr((yyvsp[-3].dir_abs_dec_), (yyvsp[-1].parameter_type_));  }
#line 3470 "Parser.c" /* yacc.c:1646  */
    break;

  case 110:
#line 1269 "c.y" /* yacc.c:1646  */
    { (yyval.stm_) = make_LabelS((yyvsp[0].labeled_stm_)); YY_RESULT_Stm_= (yyval.stm_); }
#line 3476 "Parser.c" /* yacc.c:1646  */
    break;

  case 111:
#line 1270 "c.y" /* yacc.c:1646  */
    { (yyval.stm_) = make_CompS((yyvsp[0].compound_stm_)); YY_RESULT_Stm_= (yyval.stm_); }
#line 3482 "Parser.c" /* yacc.c:1646  */
    break;

  case 112:
#line 1271 "c.y" /* yacc.c:1646  */
    { (yyval.stm_) = make_ExprS((yyvsp[0].expression_stm_)); YY_RESULT_Stm_= (yyval.stm_); }
#line 3488 "Parser.c" /* yacc.c:1646  */
    break;

  case 113:
#line 1272 "c.y" /* yacc.c:1646  */
    { (yyval.stm_) = make_SelS((yyvsp[0].selection_stm_)); YY_RESULT_Stm_= (yyval.stm_); }
#line 3494 "Parser.c" /* yacc.c:1646  */
    break;

  case 114:
#line 1273 "c.y" /* yacc.c:1646  */
    { (yyval.stm_) = make_IterS((yyvsp[0].iter_stm_)); YY_RESULT_Stm_= (yyval.stm_); }
#line 3500 "Parser.c" /* yacc.c:1646  */
    break;

  case 115:
#line 1274 "c.y" /* yacc.c:1646  */
    { (yyval.stm_) = make_JumpS((yyvsp[0].jump_stm_)); YY_RESULT_Stm_= (yyval.stm_); }
#line 3506 "Parser.c" /* yacc.c:1646  */
    break;

  case 116:
#line 1276 "c.y" /* yacc.c:1646  */
    { (yyval.labeled_stm_) = make_SlabelOne((yyvsp[-2].string_), (yyvsp[0].stm_));  }
#line 3512 "Parser.c" /* yacc.c:1646  */
    break;

  case 117:
#line 1277 "c.y" /* yacc.c:1646  */
    { (yyval.labeled_stm_) = make_SlabelTwo((yyvsp[-2].constant_expression_), (yyvsp[0].stm_));  }
#line 3518 "Parser.c" /* yacc.c:1646  */
    break;

  case 118:
#line 1278 "c.y" /* yacc.c:1646  */
    { (yyval.labeled_stm_) = make_SlabelThree((yyvsp[0].stm_));  }
#line 3524 "Parser.c" /* yacc.c:1646  */
    break;

  case 119:
#line 1280 "c.y" /* yacc.c:1646  */
    { (yyval.compound_stm_) = make_ScompOne();  }
#line 3530 "Parser.c" /* yacc.c:1646  */
    break;

  case 120:
#line 1281 "c.y" /* yacc.c:1646  */
    { (yyval.compound_stm_) = make_ScompTwo((yyvsp[-1].liststm_));  }
#line 3536 "Parser.c" /* yacc.c:1646  */
    break;

  case 121:
#line 1282 "c.y" /* yacc.c:1646  */
    { (yyval.compound_stm_) = make_ScompThree((yyvsp[-1].listdec_));  }
#line 3542 "Parser.c" /* yacc.c:1646  */
    break;

  case 122:
#line 1283 "c.y" /* yacc.c:1646  */
    { (yyval.compound_stm_) = make_ScompFour((yyvsp[-2].listdec_), (yyvsp[-1].liststm_));  }
#line 3548 "Parser.c" /* yacc.c:1646  */
    break;

  case 123:
#line 1285 "c.y" /* yacc.c:1646  */
    { (yyval.expression_stm_) = make_SexprOne();  }
#line 3554 "Parser.c" /* yacc.c:1646  */
    break;

  case 124:
#line 1286 "c.y" /* yacc.c:1646  */
    { (yyval.expression_stm_) = make_SexprTwo((yyvsp[-1].exp_));  }
#line 3560 "Parser.c" /* yacc.c:1646  */
    break;

  case 125:
#line 1288 "c.y" /* yacc.c:1646  */
    { (yyval.selection_stm_) = make_SselOne((yyvsp[-2].exp_), (yyvsp[0].stm_));  }
#line 3566 "Parser.c" /* yacc.c:1646  */
    break;

  case 126:
#line 1289 "c.y" /* yacc.c:1646  */
    { (yyval.selection_stm_) = make_SselTwo((yyvsp[-4].exp_), (yyvsp[-2].stm_), (yyvsp[0].stm_));  }
#line 3572 "Parser.c" /* yacc.c:1646  */
    break;

  case 127:
#line 1290 "c.y" /* yacc.c:1646  */
    { (yyval.selection_stm_) = make_SselThree((yyvsp[-2].exp_), (yyvsp[0].stm_));  }
#line 3578 "Parser.c" /* yacc.c:1646  */
    break;

  case 128:
#line 1292 "c.y" /* yacc.c:1646  */
    { (yyval.iter_stm_) = make_SiterOne((yyvsp[-2].exp_), (yyvsp[0].stm_));  }
#line 3584 "Parser.c" /* yacc.c:1646  */
    break;

  case 129:
#line 1293 "c.y" /* yacc.c:1646  */
    { (yyval.iter_stm_) = make_SiterTwo((yyvsp[-5].stm_), (yyvsp[-2].exp_));  }
#line 3590 "Parser.c" /* yacc.c:1646  */
    break;

  case 130:
#line 1294 "c.y" /* yacc.c:1646  */
    { (yyval.iter_stm_) = make_SiterThree((yyvsp[-3].expression_stm_), (yyvsp[-2].expression_stm_), (yyvsp[0].stm_));  }
#line 3596 "Parser.c" /* yacc.c:1646  */
    break;

  case 131:
#line 1295 "c.y" /* yacc.c:1646  */
    { (yyval.iter_stm_) = make_SiterFour((yyvsp[-4].expression_stm_), (yyvsp[-3].expression_stm_), (yyvsp[-2].exp_), (yyvsp[0].stm_));  }
#line 3602 "Parser.c" /* yacc.c:1646  */
    break;

  case 132:
#line 1297 "c.y" /* yacc.c:1646  */
    { (yyval.jump_stm_) = make_SjumpOne((yyvsp[-1].string_));  }
#line 3608 "Parser.c" /* yacc.c:1646  */
    break;

  case 133:
#line 1298 "c.y" /* yacc.c:1646  */
    { (yyval.jump_stm_) = make_SjumpTwo();  }
#line 3614 "Parser.c" /* yacc.c:1646  */
    break;

  case 134:
#line 1299 "c.y" /* yacc.c:1646  */
    { (yyval.jump_stm_) = make_SjumpThree();  }
#line 3620 "Parser.c" /* yacc.c:1646  */
    break;

  case 135:
#line 1300 "c.y" /* yacc.c:1646  */
    { (yyval.jump_stm_) = make_SjumpFour();  }
#line 3626 "Parser.c" /* yacc.c:1646  */
    break;

  case 136:
#line 1301 "c.y" /* yacc.c:1646  */
    { (yyval.jump_stm_) = make_SjumpFive((yyvsp[-1].exp_));  }
#line 3632 "Parser.c" /* yacc.c:1646  */
    break;

  case 137:
#line 1303 "c.y" /* yacc.c:1646  */
    { (yyval.liststm_) = make_ListStm((yyvsp[0].stm_), 0);  }
#line 3638 "Parser.c" /* yacc.c:1646  */
    break;

  case 138:
#line 1304 "c.y" /* yacc.c:1646  */
    { (yyval.liststm_) = make_ListStm((yyvsp[-1].stm_), (yyvsp[0].liststm_));  }
#line 3644 "Parser.c" /* yacc.c:1646  */
    break;

  case 139:
#line 1306 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Ecomma((yyvsp[-2].exp_), (yyvsp[0].exp_)); YY_RESULT_Exp_= (yyval.exp_); }
#line 3650 "Parser.c" /* yacc.c:1646  */
    break;

  case 140:
#line 1307 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_); YY_RESULT_Exp_= (yyval.exp_); }
#line 3656 "Parser.c" /* yacc.c:1646  */
    break;

  case 141:
#line 1309 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Eassign((yyvsp[-2].exp_), (yyvsp[-1].assignment_op_), (yyvsp[0].exp_));  }
#line 3662 "Parser.c" /* yacc.c:1646  */
    break;

  case 142:
#line 1310 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3668 "Parser.c" /* yacc.c:1646  */
    break;

  case 143:
#line 1312 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Econdition((yyvsp[-4].exp_), (yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3674 "Parser.c" /* yacc.c:1646  */
    break;

  case 144:
#line 1313 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3680 "Parser.c" /* yacc.c:1646  */
    break;

  case 145:
#line 1315 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Elor((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3686 "Parser.c" /* yacc.c:1646  */
    break;

  case 146:
#line 1316 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3692 "Parser.c" /* yacc.c:1646  */
    break;

  case 147:
#line 1318 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Eland((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3698 "Parser.c" /* yacc.c:1646  */
    break;

  case 148:
#line 1319 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3704 "Parser.c" /* yacc.c:1646  */
    break;

  case 149:
#line 1321 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Ebitor((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3710 "Parser.c" /* yacc.c:1646  */
    break;

  case 150:
#line 1322 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3716 "Parser.c" /* yacc.c:1646  */
    break;

  case 151:
#line 1324 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Ebitexor((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3722 "Parser.c" /* yacc.c:1646  */
    break;

  case 152:
#line 1325 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3728 "Parser.c" /* yacc.c:1646  */
    break;

  case 153:
#line 1327 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Ebitand((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3734 "Parser.c" /* yacc.c:1646  */
    break;

  case 154:
#line 1328 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3740 "Parser.c" /* yacc.c:1646  */
    break;

  case 155:
#line 1330 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Eeq((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3746 "Parser.c" /* yacc.c:1646  */
    break;

  case 156:
#line 1331 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Eneq((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3752 "Parser.c" /* yacc.c:1646  */
    break;

  case 157:
#line 1332 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3758 "Parser.c" /* yacc.c:1646  */
    break;

  case 158:
#line 1334 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Elthen((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3764 "Parser.c" /* yacc.c:1646  */
    break;

  case 159:
#line 1335 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Egrthen((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3770 "Parser.c" /* yacc.c:1646  */
    break;

  case 160:
#line 1336 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Ele((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3776 "Parser.c" /* yacc.c:1646  */
    break;

  case 161:
#line 1337 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Ege((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3782 "Parser.c" /* yacc.c:1646  */
    break;

  case 162:
#line 1338 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3788 "Parser.c" /* yacc.c:1646  */
    break;

  case 163:
#line 1340 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Eleft((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3794 "Parser.c" /* yacc.c:1646  */
    break;

  case 164:
#line 1341 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Eright((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3800 "Parser.c" /* yacc.c:1646  */
    break;

  case 165:
#line 1342 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3806 "Parser.c" /* yacc.c:1646  */
    break;

  case 166:
#line 1344 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Eplus((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3812 "Parser.c" /* yacc.c:1646  */
    break;

  case 167:
#line 1345 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Eminus((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3818 "Parser.c" /* yacc.c:1646  */
    break;

  case 168:
#line 1346 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3824 "Parser.c" /* yacc.c:1646  */
    break;

  case 169:
#line 1348 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Etimes((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3830 "Parser.c" /* yacc.c:1646  */
    break;

  case 170:
#line 1349 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Ediv((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3836 "Parser.c" /* yacc.c:1646  */
    break;

  case 171:
#line 1350 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Emod((yyvsp[-2].exp_), (yyvsp[0].exp_));  }
#line 3842 "Parser.c" /* yacc.c:1646  */
    break;

  case 172:
#line 1351 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3848 "Parser.c" /* yacc.c:1646  */
    break;

  case 173:
#line 1353 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Etypeconv((yyvsp[-2].type_name_), (yyvsp[0].exp_));  }
#line 3854 "Parser.c" /* yacc.c:1646  */
    break;

  case 174:
#line 1354 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3860 "Parser.c" /* yacc.c:1646  */
    break;

  case 175:
#line 1356 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Epreinc((yyvsp[0].exp_));  }
#line 3866 "Parser.c" /* yacc.c:1646  */
    break;

  case 176:
#line 1357 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Epredec((yyvsp[0].exp_));  }
#line 3872 "Parser.c" /* yacc.c:1646  */
    break;

  case 177:
#line 1358 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Epreop((yyvsp[-1].unary_operator_), (yyvsp[0].exp_));  }
#line 3878 "Parser.c" /* yacc.c:1646  */
    break;

  case 178:
#line 1359 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Ebytesexpr((yyvsp[0].exp_));  }
#line 3884 "Parser.c" /* yacc.c:1646  */
    break;

  case 179:
#line 1360 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Ebytestype((yyvsp[-1].type_name_));  }
#line 3890 "Parser.c" /* yacc.c:1646  */
    break;

  case 180:
#line 1361 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3896 "Parser.c" /* yacc.c:1646  */
    break;

  case 181:
#line 1363 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Earray((yyvsp[-3].exp_), (yyvsp[-1].exp_));  }
#line 3902 "Parser.c" /* yacc.c:1646  */
    break;

  case 182:
#line 1364 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Efunk((yyvsp[-2].exp_));  }
#line 3908 "Parser.c" /* yacc.c:1646  */
    break;

  case 183:
#line 1365 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Efunkpar((yyvsp[-3].exp_), (yyvsp[-1].listexp_));  }
#line 3914 "Parser.c" /* yacc.c:1646  */
    break;

  case 184:
#line 1366 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Eselect((yyvsp[-2].exp_), (yyvsp[0].string_));  }
#line 3920 "Parser.c" /* yacc.c:1646  */
    break;

  case 185:
#line 1367 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Epoint((yyvsp[-2].exp_), (yyvsp[0].string_));  }
#line 3926 "Parser.c" /* yacc.c:1646  */
    break;

  case 186:
#line 1368 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Epostinc((yyvsp[-1].exp_));  }
#line 3932 "Parser.c" /* yacc.c:1646  */
    break;

  case 187:
#line 1369 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Epostdec((yyvsp[-1].exp_));  }
#line 3938 "Parser.c" /* yacc.c:1646  */
    break;

  case 188:
#line 1370 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[0].exp_);  }
#line 3944 "Parser.c" /* yacc.c:1646  */
    break;

  case 189:
#line 1372 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Evar((yyvsp[0].string_));  }
#line 3950 "Parser.c" /* yacc.c:1646  */
    break;

  case 190:
#line 1373 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Econst((yyvsp[0].constant_));  }
#line 3956 "Parser.c" /* yacc.c:1646  */
    break;

  case 191:
#line 1374 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = make_Estring((yyvsp[0].string_));  }
#line 3962 "Parser.c" /* yacc.c:1646  */
    break;

  case 192:
#line 1375 "c.y" /* yacc.c:1646  */
    { (yyval.exp_) = (yyvsp[-1].exp_);  }
#line 3968 "Parser.c" /* yacc.c:1646  */
    break;

  case 193:
#line 1377 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Efloat((yyvsp[0].double_));  }
#line 3974 "Parser.c" /* yacc.c:1646  */
    break;

  case 194:
#line 1378 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Echar((yyvsp[0].char_));  }
#line 3980 "Parser.c" /* yacc.c:1646  */
    break;

  case 195:
#line 1379 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Eunsigned((yyvsp[0].string_));  }
#line 3986 "Parser.c" /* yacc.c:1646  */
    break;

  case 196:
#line 1380 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Elong((yyvsp[0].string_));  }
#line 3992 "Parser.c" /* yacc.c:1646  */
    break;

  case 197:
#line 1381 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Eunsignlong((yyvsp[0].string_));  }
#line 3998 "Parser.c" /* yacc.c:1646  */
    break;

  case 198:
#line 1382 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Ehexadec((yyvsp[0].string_));  }
#line 4004 "Parser.c" /* yacc.c:1646  */
    break;

  case 199:
#line 1383 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Ehexaunsign((yyvsp[0].string_));  }
#line 4010 "Parser.c" /* yacc.c:1646  */
    break;

  case 200:
#line 1384 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Ehexalong((yyvsp[0].string_));  }
#line 4016 "Parser.c" /* yacc.c:1646  */
    break;

  case 201:
#line 1385 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Ehexaunslong((yyvsp[0].string_));  }
#line 4022 "Parser.c" /* yacc.c:1646  */
    break;

  case 202:
#line 1386 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Eoctal((yyvsp[0].string_));  }
#line 4028 "Parser.c" /* yacc.c:1646  */
    break;

  case 203:
#line 1387 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Eoctalunsign((yyvsp[0].string_));  }
#line 4034 "Parser.c" /* yacc.c:1646  */
    break;

  case 204:
#line 1388 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Eoctallong((yyvsp[0].string_));  }
#line 4040 "Parser.c" /* yacc.c:1646  */
    break;

  case 205:
#line 1389 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Eoctalunslong((yyvsp[0].string_));  }
#line 4046 "Parser.c" /* yacc.c:1646  */
    break;

  case 206:
#line 1390 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Ecdouble((yyvsp[0].string_));  }
#line 4052 "Parser.c" /* yacc.c:1646  */
    break;

  case 207:
#line 1391 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Ecfloat((yyvsp[0].string_));  }
#line 4058 "Parser.c" /* yacc.c:1646  */
    break;

  case 208:
#line 1392 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Eclongdouble((yyvsp[0].string_));  }
#line 4064 "Parser.c" /* yacc.c:1646  */
    break;

  case 209:
#line 1393 "c.y" /* yacc.c:1646  */
    { (yyval.constant_) = make_Eint((yyvsp[0].int_));  }
#line 4070 "Parser.c" /* yacc.c:1646  */
    break;

  case 210:
#line 1395 "c.y" /* yacc.c:1646  */
    { (yyval.constant_expression_) = make_Especial((yyvsp[0].exp_));  }
#line 4076 "Parser.c" /* yacc.c:1646  */
    break;

  case 211:
#line 1397 "c.y" /* yacc.c:1646  */
    { (yyval.unary_operator_) = make_Address();  }
#line 4082 "Parser.c" /* yacc.c:1646  */
    break;

  case 212:
#line 1398 "c.y" /* yacc.c:1646  */
    { (yyval.unary_operator_) = make_Indirection();  }
#line 4088 "Parser.c" /* yacc.c:1646  */
    break;

  case 213:
#line 1399 "c.y" /* yacc.c:1646  */
    { (yyval.unary_operator_) = make_Plus();  }
#line 4094 "Parser.c" /* yacc.c:1646  */
    break;

  case 214:
#line 1400 "c.y" /* yacc.c:1646  */
    { (yyval.unary_operator_) = make_Negative();  }
#line 4100 "Parser.c" /* yacc.c:1646  */
    break;

  case 215:
#line 1401 "c.y" /* yacc.c:1646  */
    { (yyval.unary_operator_) = make_Complement();  }
#line 4106 "Parser.c" /* yacc.c:1646  */
    break;

  case 216:
#line 1402 "c.y" /* yacc.c:1646  */
    { (yyval.unary_operator_) = make_Logicalneg();  }
#line 4112 "Parser.c" /* yacc.c:1646  */
    break;

  case 217:
#line 1404 "c.y" /* yacc.c:1646  */
    { (yyval.listexp_) = make_ListExp((yyvsp[0].exp_), 0);  }
#line 4118 "Parser.c" /* yacc.c:1646  */
    break;

  case 218:
#line 1405 "c.y" /* yacc.c:1646  */
    { (yyval.listexp_) = make_ListExp((yyvsp[-2].exp_), (yyvsp[0].listexp_));  }
#line 4124 "Parser.c" /* yacc.c:1646  */
    break;

  case 219:
#line 1407 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_Assign();  }
#line 4130 "Parser.c" /* yacc.c:1646  */
    break;

  case 220:
#line 1408 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignMul();  }
#line 4136 "Parser.c" /* yacc.c:1646  */
    break;

  case 221:
#line 1409 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignDiv();  }
#line 4142 "Parser.c" /* yacc.c:1646  */
    break;

  case 222:
#line 1410 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignMod();  }
#line 4148 "Parser.c" /* yacc.c:1646  */
    break;

  case 223:
#line 1411 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignAdd();  }
#line 4154 "Parser.c" /* yacc.c:1646  */
    break;

  case 224:
#line 1412 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignSub();  }
#line 4160 "Parser.c" /* yacc.c:1646  */
    break;

  case 225:
#line 1413 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignLeft();  }
#line 4166 "Parser.c" /* yacc.c:1646  */
    break;

  case 226:
#line 1414 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignRight();  }
#line 4172 "Parser.c" /* yacc.c:1646  */
    break;

  case 227:
#line 1415 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignAnd();  }
#line 4178 "Parser.c" /* yacc.c:1646  */
    break;

  case 228:
#line 1416 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignXor();  }
#line 4184 "Parser.c" /* yacc.c:1646  */
    break;

  case 229:
#line 1417 "c.y" /* yacc.c:1646  */
    { (yyval.assignment_op_) = make_AssignOr();  }
#line 4190 "Parser.c" /* yacc.c:1646  */
    break;


#line 4194 "Parser.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
