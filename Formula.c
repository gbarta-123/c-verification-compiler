#include <stdio.h>
#include <stdlib.h>
#include "Formula.h"
#include "Mem.h"

Formula mk_stmt_formula(Statement _stmt, Formula _next, Formula _prev)
{
	Formula formula = pmalloc(sizeof(*formula));
	formula->type = STMT;
	formula->u.stmt = _stmt;
	formula->next = _next;
	formula->prev = _prev;
	return formula;
}

Formula mk_connective_formula(Operator _connective,
			      Formula _next, Formula _prev)
{
	Formula formula = pmalloc(sizeof(*formula));
	formula->type = CONNECTIVE;
	formula->u.connective = _connective;
	formula->next = _next;
	formula->prev = _prev;
	return formula;

}

Formula add_preimpl_formula(Formula pre, Formula post)
{
	Formula pre_temp = __find_last_formula(pre);
	Formula post_temp = __find_first_formula(post);
	Formula impl_frml = mk_connective_formula(mk_logop_operator(IMPL),
						  post_temp, pre_temp);
	return __find_first_formula(impl_frml);

}

Formula push_back_formula(Formula pre, Formula post)
{
	Formula pre_temp = __find_last_formula(pre);
	Formula post_temp = __find_first_formula(post);
	pre_temp->next = post_temp;
	post_temp->prev = pre_temp;
	return __find_first_formula(pre_temp);
}

void print_formulae(Formula formula)
{
	Formula tmp = __find_first_formula(formula);
	while(tmp->next != NULL) {
		__print_formula(tmp);
		tmp = tmp->next;
	}
}

Formula __find_first_formula(Formula formula)
{
	Formula tmp = formula;
	while(tmp->prev != NULL)
		tmp = tmp->prev;
	return tmp;
}

Formula __find_last_formula(Formula formula)
{
	Formula tmp = formula;
	while(tmp->next != NULL)
		tmp = tmp->next;
	return tmp;
}

void __print_formula(Formula formula)
{
	switch(formula->type) {
	case STMT:
		print_statement(formula->u.stmt);
		break;
	case CONNECTIVE:
		print_operator(formula->u.connective);
		break;
	default:
		fprintf(stderr, "UNDECLARED FORMULA TYPE!\n");
		exit(EXIT_FAILURE);
	}
}
