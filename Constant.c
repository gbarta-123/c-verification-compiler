#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Constant.h"
#include "Mem.h"

Constant mk_named_constant(bitvect_t _value, char *_name)
{
	Constant constant = pmalloc(sizeof(*constant));
	constant->type = NAMED;
	constant->u.named.value = _value;
	constant->u.named.name = _name;
	return constant;
}

Constant mk_unnamed_constant(bitvect_t _value)
{
	Constant constant = pmalloc(sizeof(*constant));
	constant->type = UNNAMED;
	constant->u.unnamed.value = _value;
	return constant;
}

void print_constant(Constant constant)
{
	switch(constant->type) {
	case NAMED:
		print_bitvect(constant->u.named.value);
		break;
	case UNNAMED:
		print_bitvect(constant->u.unnamed.value);
		break;
	default:
		fprintf(stderr, "UNDEFINED CONSTANT TYPE!\n");
		exit(EXIT_FAILURE);
	}
}
