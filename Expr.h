#ifndef EXPR_H
#define EXPR_H

#include "Operators.h"
#include "Array.h"
#include "defines.h"

struct Expr_ {
	enum { OPER, NUM, IDENT, ARRAY } type;
	union {
		Operator oper;
		int num;
		char *ident;
		Array array;
	} u;
};

Expr mk_oper_expr(Operator);
Expr mk_num_expr(int);
Expr mk_ident_expr(char *);
Expr mk_array_expr(Array);

void print_expr(Expr);

#endif // EXPR_H
