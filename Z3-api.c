#ifndef Z3_API
#define Z3_API

#include <z3.h>
#include <setjmp.h>
#include <stdlib.h>

#define LOG_Z3_CALLS

#ifdef LOG_Z3_CALLS
#define LOG_MSG(msg) Z3_append_log(msg)
#else
#define LOG_MSG(msg) ((void)0)
#endif

static jmp_buf g_catch_buffer;

/**
 * \brief Exit with error message. 
 */
void exitf(const char* message);
/**
 * \brief Exit if unreachable code was reached.
 */
void unreachable();
/**
 * \brief Simpler error handler.
 */
void error_handler(Z3_context c, Z3_error_code e);
/**
 * \brief Exception throwing capability.
 */
void throw_z3_error(Z3_context c, Z3_error_code e);
/**
 * \brief Create a logical context.
 *  Enable fine-grained proof construction.
 *  Enable model construction.
 *  Also enable tracing to stderr and register standard error handler.
 */
Z3_context mk_proof_context();
/**
 * \brief Create a logical context.
 *  Enable model construction. 
 *  Other configuration parameters can be passed in the cfg variable.
 *  Also enable tracing to stderr and register custom error handler.
 */
Z3_context mk_context_custom(Z3_config cfg, Z3_error_handler err);
/**
 * \brief Create a variable using the given name and type.
 */
Z3_ast mk_var(Z3_context ctx, const char * name, Z3_sort ty);
/**
 * \brief Create a boolean variable using the given name.
 */
Z3_ast mk_bool_var(Z3_context ctx, const char * name);
/**
 * \brief Creates solver. 
 */
Z3_solver ml_solver(Z3_context ctx);
/**
 * \brief Deletes solver. 
 */
void del_solver(Z3_context ctx, Z3_solver s);
/**
 * \brief Check whether the logical context is satisfiable, 
 *  and compare the result with the expected result.
 *  If the context is satisfiable, then display the model.
 */
void check(Z3_context ctx, Z3_solver s, Z3_lbool expected_result);
/**
 * \brief Custom ast pretty printer.
 * This function demonstrates how to use the API to navigate terms.
 */
void display_ast(Z3_context c, FILE * out, Z3_ast v);
/**
 * \brief Display the given type.
 */
void display_sort(Z3_context c, FILE * out, Z3_sort ty);
/**
 *  \brief Display a symbol in the given output stream.
 */
void display_symbol(Z3_context c, FILE * out, Z3_symbol s);

#endif // Z3_API
