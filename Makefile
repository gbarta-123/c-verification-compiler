CC = gcc
CCFLAGS = -g -W -Wall

FLEX = flex
FLEX_OPTS = -Pc_

BISON = bison
BISON_OPTS = -t -pc_

OBJS = Absyn.o Lexer.o Parser.o Printer.o Array.o Expr.o ArExpr.o Mem.o Operators.o Path.o Statement.o Function.o Constant.o Bitvect.o PContainer.o Skeleton.o

.PHONY: clean distclean

all: Testc

clean:
	rm -f *.o Testc c.aux c.log c.pdf c.dvi c.ps c

distclean: clean
	rm -f Absyn.h Absyn.c Test.c Parser.c Parser.h Lexer.c Skeleton.c Skeleton.h Printer.c Printer.h Makefile c.l c.y c.tex 

Testc: ${OBJS} Test.o
	@echo "Linking Testc..."
	${CC} ${CCFLAGS} ${OBJS} Test.o -o Testc
	mv *.o Testc build/

Absyn.o: Absyn.c Absyn.h
	${CC} ${CCFLAGS} -c Absyn.c

Skeleton.o: Skeleton.c Skeleton.h Absyn.h PContainer.h
	${CC} ${CCFLAGS} -c Skeleton.c

PContainer.o: PContainer.c PContainer.h Function.h defines.h Mem.h
	${CC} ${CCFALGS} -c PContainer.c

Bitvect.o: Bitvect.c Bitvect.h defines.h
	${CC} ${CCFLAGS} -c Bitvect.c

Cosntant.o: Constant.c Constant.h Bitvect.h Mem.h defines.h
	${CC} ${CCFALGS} -c Constant.c

Function.o: Function.c Function.h Path.h defines.h Mem.h
	${CC} ${CCFLAGS} -c Function.c

Path.o: Path.c Path.h Formula.h Statement.h Mem.h defines.h
	${CC} ${CCFLAGS} -c Path.c

Statement.o: Statement.c Statement.h ArExpr.h Operators.h defines.h
	${CC} ${CCFLAGS} -c Statement.c

Operators.o: Operators.c Operators.h Mem.h defines.h
	${CC} ${CCFLAGS} -c Operators.c

Mem.o: Mem.c Mem.h
	${CC} ${CCFLAGS} -c Mem.c

ArExpr.o: ArExpr.c ArExpr.h Expr.h Mem.h defines.h
	${CC} ${CCFLAGS} -c ArExpr.c

Expr.o: Expr.c Expr.h Operators.h Array.h Mem.h defines.h
	${CC} ${CCFLAGS} -c Expr.c

Array.o: Array.c Array.h defines.h Statement.h Mem.h defines.h
	${CC} ${CCFLAGS} -c Array.c

Lexer.c: c.l
	${FLEX} ${FLEX_OPTS} -oLexer.c c.l

Parser.c: c.y
	${BISON} ${BISON_OPTS} c.y -o Parser.c

Lexer.o: Lexer.c Parser.h
	${CC} ${CCFLAGS} -c Lexer.c 

Parser.o: Parser.c Absyn.h
	${CC} ${CCFLAGS} -c Parser.c

Printer.o: Printer.c Printer.h Absyn.h
	${CC} ${CCFLAGS} -c Printer.c

Test.o: Test.c Parser.h Printer.h Absyn.h
	${CC} ${CCFLAGS} -c Test.c

