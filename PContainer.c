#include <stdio.h>
#include <stdlib.h>
#include "PContainer.h"
#include "Mem.h"

PContainer mk_pcontainer()
{
	PContainer progr = pmalloc(sizeof(*progr));
	progr->funcs = pmalloc(sizeof(*(progr->funcs)) * REALLOC_QUANTUM);
	progr->stst = mk_statusstack();
	progr->reall_q = 1;
	progr->n_funcs = 0;
	return progr;
}

void add_func_pcontainer(PContainer progr, Function func)
{
	if(unlikely((REALLOC_QUANTUM * progr->reall_q) < (progr->n_funcs))) {
		size_t new_length = sizeof(*(progr->funcs))
			* (++(progr->reall_q))
			* REALLOC_QUANTUM;
		prealloc(progr->funcs, new_length);
	}
	progr->funcs[progr->n_funcs++] = func;
	push_status(progr->stst, FUNC);
}

void print_pcontainer(PContainer pcont)
{
	printf("\n[[BASIC PATH CONTAINER]]\n");
	for(size_t i = 0; i < pcont->n_funcs; ++i) {
		Function cfunc = pcont->funcs[i];
		printf("[PATH FOR FUNCTION %s]", cfunc->name);
		print_function(cfunc);
	}
	printf("\n\n");
}

StatusStack mk_statusstack()
{
	StatusStack stack = pmalloc(sizeof(*stack));
	stack->sp = -1;
	return stack;
}

void push_status(StatusStack stst, enum status _st)
{
	if(stst->sp > 100) {
		fprintf(stderr, "Status Stack Overflow!\n");
		exit(EXIT_FAILURE);
	}
	stst->st[stst->sp++] = _st;
}

enum status pop_status(StatusStack stst)
{
	if(stst->sp < 0) {
		fprintf(stderr, "Status Stack Underflow\n");
		exit(EXIT_FAILURE);
	}
	return stst->st[stst->sp--];
}

enum status get_top_stack_status(StatusStack stst)
{
	return stst->st[stst->sp];
}

void add_int_pcont(PContainer pc, int integer)
{
	switch(get_top_stack_status(pc->stst)) {
	case AREXPR1:
		add_expr_arexpr(
			current_expr1,
			mk_num_expr(integer)
		);
		break;
	case AREXPR2:
		add_expr_arexpr(
			current_expr2,
			mk_num_expr(integer)
		);
		break;
	default:
		fprintf(stderr,
			"Wrong order of arithmetic operations (prefix needed)!\n"
		);
		exit(EXIT_FAILURE);

	}
}

void add_oper_pcont(PContainer pc, Operator oper)
{
	if(get_top_stack_status(pc->stst) != AREXPR)
		push_status(pc->stst, AREXPR);
	add_expr_arexpr(
		current_expr,
		mk_oper_expr(oper)
	);
}

void submit_erexpr_pcont(PContainer pc)
{
	if(pop_status(pc->stst) == AREXPR)
		print_arexpr(current_expr);
	else {
		fprintf(stderr, "No expression to submit!\n");
		exit(EXIT_FAILURE);
	}
}
