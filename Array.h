#ifndef ARRAY_H
#define ARRAY_H

#include <stddef.h>
#include "defines.h"
#include "Statement.h"
#include "defines.h"

struct Array_ {
	enum { STORE, SELECT } type;
	union {
		struct {
			char *name;
			size_t idx;
			Statement stmt;
		} store;
		struct {
			char *name;
			size_t idx;
		} select;
	} u;
};

Array mk_store_array(char *, size_t, Statement);
Array mk_select_array(char *, size_t);

void print_array(Array);
#endif // ARRAY_H
