#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Array.h"
#include "Mem.h"

Array mk_store_array(char *_name, size_t _idx, Statement _stmt)
{
	Array array = pmalloc(sizeof(*array));
	array->type = STORE;
	array->u.store.name = _name;
	array->u.store.idx = _idx;
	array->u.store.stmt = _stmt;
	return array;
}

Array mk_select_array(char *_name, size_t _idx)
{
	Array array = pmalloc(sizeof(*array));
	array->type = SELECT;
	array->u.select.name = _name;
	array->u.select.idx = _idx;
	return array;
}

void print_array(Array array)
{
	switch(array->type) {
	case STORE:
		printf(" %s[%zu] = ", array->u.store.name, array->u.store.idx);
		print_statement(array->u.store.stmt);
		break;
	case SELECT:
		printf(" %s[%zu] ", array->u.select.name, array->u.select.idx);
		break;
	default:
		fprintf(stderr, "ARRAY TYPE NOT DEFINED\n");
		exit(EXIT_FAILURE);
	}
}
