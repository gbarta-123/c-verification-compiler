#ifndef DEFINES_H
#define DEFINES_H

/**
 * Constants
 */
#define REALLOC_QUANTUM 100
/**
 * Enhance performance for if-else conditions.
 */
#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)
/**
 * Add prover typedefs to break circular dependencies.
 */
typedef struct Statement_ *Statement;
typedef struct ArExpr_ *ArExpr;
typedef struct Expr_ *Expr;
typedef struct Array_ *Array;
typedef struct Statement_ *Statement;
typedef struct bitvect bitvect_t;
typedef struct Constant_ *Constant;
typedef struct Formula_ *Formula;
typedef struct Operator_ *Operator;
typedef struct Path_ *Path;
typedef struct Function_ *Function;
typedef struct PContainer_ *PContainer;
typedef struct StatusStack_ *StatusStack;
#endif // DEFINES_H
