#include "ArExpr.h"
#include "Mem.h"

ArExpr mk_arexpr()
{
	ArExpr arexpr = pmalloc(sizeof(*arexpr));
	arexpr->exprs = pmalloc(sizeof(*(arexpr->exprs)) * REALLOC_QUANTUM);
	arexpr->reall_q = 1;
	arexpr->length = 0;
	return arexpr;
}

void add_expr_arexpr(ArExpr _arexpr, Expr _expr)
{
	// SIMILAR TO add_path_function IN function.h
	if(unlikely((REALLOC_QUANTUM * _arexpr->reall_q) < (_arexpr->length + 1))) {
		size_t new_length = sizeof(*(_arexpr->exprs))
			* (++(_arexpr->reall_q))
			* REALLOC_QUANTUM;
		prealloc(_arexpr->exprs, new_length);
	}
	_arexpr->exprs[_arexpr->length++] = _expr;
}

void print_arexpr(ArExpr arexpr)
{
	for(size_t i = 0; i < arexpr->length; ++i)
		print_expr(arexpr->exprs[i]);
}
