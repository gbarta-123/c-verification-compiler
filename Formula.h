#ifndef FORMULA_H
#define FORMULA_H

#include "Operators.h"
#include "Statement.h"
#include "defines.h"

struct Formula_ {
	enum { STMT, CONNECTIVE } type;
	union {
		Statement stmt;
		Operator connective;
	} u;
	Formula next, prev;
};

Formula mk_stmt_formula(Statement, Formula, Formula);
Formula mk_connective_formula(Operator, Formula, Formula);
Formula push_back_formula(Formula, Formula);
Formula add_preimpl_formula(Formula, Formula);
// returns the first/last element of the list
Formula __find_first_formula(Formula);
Formula __find_last_formula(Formula);

void __print_formula(Formula);
void print_formulae(Formula);

#endif // FORMULA_H
