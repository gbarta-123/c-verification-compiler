#include "Mem.h"
#include <stdio.h>
#include <stdlib.h>

void *pmalloc(size_t size)
{
    void *ptr = malloc(size);
    if(ptr != NULL)
        return ptr;
    fprintf(stderr, "Error: Not enough memory\n");
    exit(EXIT_FAILURE);
}

void pfree(void *ptr)
{
    if(ptr != NULL)
        free(ptr);
    else {
        fprintf(stderr, "Error: Ptr has been already freed\n");
        exit(EXIT_FAILURE);
    }
}

void *prealloc(void *ptr, size_t size)
{
    void *n_ptr = realloc(ptr, size);
    if(n_ptr != NULL) {
        pfree(n_ptr);
	return ptr;
    }
    fprintf(stderr, "Error: Not enough memory\n");
    exit(EXIT_FAILURE);
}
