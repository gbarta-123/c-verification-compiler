#ifndef BITVECT_H
#define BITVECT_H

#include "defines.h"

enum typespec {
	INTEGER, UINT
};

struct bitvect {
	enum typespec type;
	union {
		int integer;
		unsigned int uint;
	} u;

};

void print_bitvect(bitvect_t);

#endif // BITVECT_H
