#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Expr.h"
#include "Mem.h"

Expr mk_oper_expr(Operator _oper)
{
	Expr expr = pmalloc(sizeof(*expr));
	expr->type = OPER;
	expr->u.oper = _oper;
	return expr;
}

Expr mk_num_expr(int _num)
{
	Expr expr = pmalloc(sizeof(*expr));
	expr->type = NUM;
	expr->u.num = _num;
	return expr;
}

Expr mk_ident_expr(char *_ident)
{
	Expr expr = pmalloc(sizeof(*expr));
	expr->type = IDENT;
	expr->u.ident = _ident;
	return expr;
}

Expr mk_array_expr(Array _array)
{
	Expr expr = pmalloc(sizeof(*expr));
	expr->type = ARRAY;
	expr->u.array = _array;
	return expr;
}

void print_expr(Expr expr)
{
	switch(expr->type) {
	case OPER:
		print_operator(expr->u.oper);
		break;
	case NUM:
		printf(" %d ", expr->u.num);
		break;
	case IDENT:
		printf(" %s ", expr->u.ident);
		break;
	case ARRAY:
		print_array(expr->u.array);
		break;
	default:
		fprintf(stderr, "UNDEFINED EXPRESSION TYPE!\n");
		exit(EXIT_FAILURE);
	}
}
