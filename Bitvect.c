#include <stdlib.h>
#include <stdio.h>
#include "Bitvect.h"

void print_bitvect(bitvect_t _bitvect)
{
	switch(_bitvect.type) {
	case INTEGER:
		printf(" %d ", _bitvect.u.integer);
		break;
	case UINT:
		printf(" %u ", _bitvect.u.uint);
		break;
	default:
		fprintf(stderr, "UNDEFINED BITVECTOR TYPE!\n");
		exit(EXIT_FAILURE);
	}
}
