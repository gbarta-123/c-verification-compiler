#ifndef OPERATORS_H
#define OPERATORS_H

#include "defines.h"

enum arop { PLUS, MINUS, MUL };

enum relop { GT, LT, GTE, LTE };

enum logop {
	AND, OR, NOT, IMPL, IFF
};

struct Operator_ {
	enum { REL, AR, LOG } type;
	union {
		enum relop rel;
		enum arop ar;
		enum logop log;
	} u;
};

Operator mk_relop_operator(enum relop);
Operator mk_arop_operator(enum arop);
Operator mk_logop_operator(enum logop);

void print_operator(Operator);
void __print_arop(enum arop);
void __print_relop(enum relop);
void __print_logop(enum logop);
#endif // OPERATORS_H
